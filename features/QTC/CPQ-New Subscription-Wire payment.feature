@SMOKE
Feature: Validate the New Subscription creation from CPQ through Wire payment
	Background:
		Given user opens oracle Bigmachine home page
	Scenario Outline: Validate submission of Quote in CPQ by selecting the wire payment mode
		When User enters valid credentails to login
		Then user verify the Oracle Quote to Order - Manager present under Home Page
		And User clicks on Quote to Order - Manager link
		Then User verify the Quote to Order - Manager page displayed
		When User click on New Transaction button
		Then user verify the system lands on the Quote page
		When User click on selectAccount button
		Then System should display the Select Account search page
		When User search for "<FirstName>","<LastName>","<Email>","<UNC>" account
		And System should display selected account information below "<FirstName>","<LastName>","<Email>"
		When User click on Quote Order Details link
		Then verify the Order Status "Created" under the Order details
		And QuoteOrdernumber is Autopopulated
		And QuoteType is "New"
		And Business Unit is "US Business Unit"
		And QuoteExpiry Date is autopopulated
		When User click on add_line_item button
		Then System should display the Teaching Resources page
		When User click on Teachables Subscriptions button
		Then System should display Model Configuration page
		When user select value "<OldISBNCode>" from oLDISBN drop down
		And User click on add_to_transaction button
		Then System should display the selected subscription "<OldISBNCode>" under line items
		When User click on Quote Order Details link
		Then verify the Order Status "Created" under the Order details
		When User click on save button
		Then User should able to see the New Quote number
		And User should also able to see the Submit for Approval button
		When User click on submit_for_approval button
		Then verify the Order Status "Approved" under the Order details
		When User click on validation_check button
		Then System should display "This Quote is on HOLD. Please review the HOLD tab"
		When User click on cancel_approvalsrevise button
		And User click on Hold Details link
		And User Selects the Checkboxes and Enter Release reason code and release comments
		And User click on release_holds button
		And User click on submit_for_approval button
		And User click on validation_check button
		And User click on submit_order button
		And User click on QuoteOrder Details link
		Then verify the Order Status "Closed" under the Order details
		When User clicks on Quote to Order - Manager link
		And  User clicks logout
		When User logged in the OSS system with valid credentials
		And User click on btnActive button
		Then System should display Subscription Management on home page
		And User click on itemNode_SubscriptionManagement_SubscriptionManagement button
		And User search for the Subscription ID which is created and closed in CPQ
		Then System should able to retrive the search subscription details successfully

		Examples:
			| FirstName  | LastName  |Email                |UNC             |Paymentmode       |OldISBNCode   | OrderTerm |
			| VEN        | BON       |venkat181@gmail.com  |1352150451       |Wire		       |9780545804844 | 2- DAYS   |
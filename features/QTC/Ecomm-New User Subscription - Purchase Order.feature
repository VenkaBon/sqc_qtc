@SMOKE
Feature: Validate the New Subscription for a new Customer from MyScholastic
	Scenario Outline: verify new user can register and subscribe as a order subscription with PO Order
		Given user lanuch the MyScholastic site
		When user clicks on register link in myScholastic
		And user enters valid credentials to register in registration screen
		And user clicks on role drop down to select a role
		And user selects single grade on role selection screen
		And user selects an school type  on school selection screen
		And user enters zipcode
		And user enters school data
		And user is on registration complete screen for TCB teacher
    And user navigates to subscription page "<URL>"
		And user clicks on POpaymentmode
		Then user clicks on Start Now CTA
    When user opens oracle Bigmachine home page
    And User enters valid credentails to login
    Then user verify the Oracle Quote to Order - Manager present under Home Page
    And User clicks on Quote to Order - Manager link
    Then User verify the Quote to Order - Manager page displayed
    And User click on search button
    And Search for order source number submitted in teachables
    And User click on QuoteOrder Details link
    And user verify the search order details populated in quote page
    And user capture the Quote number
    Then System should display Subscription Management on home page
    And User click on itemNode_SubscriptionManagement_SubscriptionManagement button
    And User search for the Subscription ID which is created in ecomm and flew through CPQ in OSS
    Then System should able to retrive the search subscription details successfully
        Examples:
                | URL |
                | https://teachables-qa.scholastic.com/on/demandware.store/Sites-tcb-us-Site/default/Cart-AddSubscriptionToBasket?partNumber=9780545804868-tcb-us&amp;amp;quantity=1&amp    |

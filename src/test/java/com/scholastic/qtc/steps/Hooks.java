package com.scholastic.qtc.steps;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

import com.scholastic.qtc.pages.ecomm_page;
import org.apache.http.client.ClientProtocolException;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.testng.annotations.BeforeClass;

import com.applitools.eyes.BatchInfo;
import com.applitools.eyes.Eyes;
import com.saucelabs.saucerest.SauceREST;
import com.scholastic.torque.common.ScreenshotUtil;
import com.scholastic.torque.common.TestBase;
import com.scholastic.torque.common.TestBaseProvider;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;

public class Hooks {

	private static boolean handledFile = false;
	public static PrintWriter writer;
	public static Eyes eyes;
	public static Map<String, Eyes> myMap = new HashMap<String, Eyes>();
	public static StringBuilder strBldr = new StringBuilder();
	String status = null;
	TestBase testBase = TestBaseProvider.getTestBase();


//	@BeforeClass
//	public static void beforeClass() throws FileNotFoundException {
//		System.out.println(":::::::::::::: Before Class");
//		if (!handledFile) {
//			writer = new PrintWriter(".//ExeComments.txt");
//			writer.print("");
//		}
//		handledFile = true;
//	}

	@Before
	public void beforeHook(Scenario scenario) throws FileNotFoundException, InterruptedException {
		synchronized (this) {
			Thread.sleep(500);
			testBase.getContext().subset("testexecution").clear();
			String session = testBase.getSessionID();
			//System.setProperty("webdriver.firefox.marionette", "servers/geckodriver.exe");
			System.out.println("########################################Environment: "
					+ testBase.getContext().getString("resources") + "############# Running scripts having tag: "
					+ testBase.getContext().getString("tags") + "############# On URL: "
					+ testBase.getContext().getString("url") + "############# XML Config: "
					+ testBase.getContext().getString("testNG.suiteXmlFile")
					+ "########################################");

			if (!session.equalsIgnoreCase("") && !testBase.getContext().getString("lambda").equalsIgnoreCase("false")) {
				String sname = scenario.getName();
				((JavascriptExecutor) testBase.getDriver()).executeScript("lambda-name=" + sname);
			}

			testBase.getContext().setProperty("ScenarioName", scenario.getName());
			System.out.println(scenario.getName());
			TestBaseProvider.getTestBase().getContext().getString("driver.platform");
			testBase.getDriver().manage().deleteAllCookies();

			if ((TestBaseProvider.getTestBase().getContext().getString("driver.platform")
					.equalsIgnoreCase("Desktop"))) {
				TestBaseProvider.getTestBase().getDriver().manage().window().maximize();
			}

			/*
			 * if (testBase.getContext().getString("applitools").equalsIgnoreCase("true")) {
			 * testBase.getContext().setProperty("SESSION", session);
			 * testBase.getContext().setProperty("SCENARIO_NAME", scenario.getName() + "-" +
			 * testBase.getContext().getString("driver.name")); Eyes eyes = new Eyes();
			 * eyes.setApiKey(testBase.getContext().getString("applitools.apiKey"));
			 *
			 * System.out.println("api key" +
			 * testBase.getContext().getString("applitools.apiKey")); BatchInfo batch;
			 *
			 * if (testBase.getContext().getString("jenkins").equalsIgnoreCase("true")) {
			 * //batch = new BatchInfo("Classroom Magazines visual testing"); batch = new
			 * BatchInfo("ECP visual testing"); String batchId =
			 * System.getenv("APPLITOOLS_BATCH_ID"); if (batchId != null) {
			 * batch.setId(batchId); } System.out.println("Batch Id: " + batchId);
			 *
			 * } else { batch = new
			 * BatchInfo(TestBaseProvider.getTestBase().getContext().getString("batchInfo"))
			 * ;
			 * batch.setId(TestBaseProvider.getTestBase().getContext().getString("batchId"))
			 * ; System.out.println("Batch Id: " +
			 * TestBaseProvider.getTestBase().getContext().getString("batchId")); }
			 * eyes.setBatch(batch);
			 */
			//ApplitoolsUtils.setEyeObject(eyes);

			//System.out.println("Batch Id: " + TestBaseProvider.getTestBase().getContext().getString("batchId"));

		}

	}

	@After
	public void afterHook(Scenario scenario) throws URISyntaxException, ClientProtocolException, IOException {
		synchronized (this) {

			WebDriver driver = TestBaseProvider.getTestBase().getDriver();
			if (scenario.isFailed()) {
				System.out.println("Method Failed::::::::::::" + TestBaseProvider.getTestBase().getString("Method"));
				try {
					scenario.write("Current Page URL is " + driver.getCurrentUrl());
				} catch (WebDriverException somePlatformsDontSupportScreenshots) {
					System.err.println(somePlatformsDontSupportScreenshots.getMessage());
				}
			}

			String session = TestBaseProvider.getTestBase().getSessionID();
			TestBase testBase = TestBaseProvider.getTestBase();
			// if( testBase.getContext().getString("lambdaTest").equalsIgnoreCase("true"))
			try {
				if (!scenario.isFailed()) {
					status = "passed";
					((JavascriptExecutor) driver).executeScript("lambda-status=" + status);
					String sname = scenario.getName();
					((JavascriptExecutor) testBase.getDriver()).executeScript("lambda-name=" + sname);

				} else {
					if (status == null) {
						status = "failed";
						((JavascriptExecutor) driver).executeScript("lambda-status=" + status);
						String sname = scenario.getName();
						((JavascriptExecutor) testBase.getDriver()).executeScript("lambda-name=" + sname);
						ScreenshotUtil.takeScreenshot(scenario);
					} else {
						status = "skipped";
						((JavascriptExecutor) driver).executeScript("lambda-status=" + status);
						String sname = scenario.getName();
						((JavascriptExecutor) testBase.getDriver()).executeScript("lambda-name=" + sname);

					}
				}

				if (session.isEmpty() || session == null) {
					session = "NoSession/TC is Skipped";
				}
				System.out.println("Session id is: " + session);
				System.out.println("ScenarioFailed=" + scenario.getName() + "<>Session=" + session + "<>Status="
						+ status + "<>Platform=" + testBase.getString("driver.name"));
				System.out.println("LambdaOnDemandSessionID=" + session + " job-name=" + scenario.getName());

				status = null;
			} catch (WebDriverException e) {
				System.out.println("<<<<9<<<<<<<<<pass>>>>>>>9>>>");
			}
			ScreenshotUtil.takeScreenshot(scenario);
//			TestBaseProvider.getTestBase().tearDown();
		}
	}

}
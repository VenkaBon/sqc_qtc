package com.scholastic.qtc.steps;


import com.scholastic.qtc.pages.cpq_Page;
import com.scholastic.qtc.pages.ecomm_page;
import com.scholastic.qtc.support.CommonUtil;
import com.scholastic.qtc.support.FieldsDto;
import com.scholastic.torque.common.ScreenshotUtil;
import com.scholastic.torque.common.TestBaseProvider;
import com.scholastic.torque.common.WrapperFunctions;
import com.torque.automation.core.TestDataUtils;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.apache.commons.lang.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import java.util.List;
import java.util.logging.Logger;

import static com.scholastic.torque.common.Robot.clickUsingJQuery;
import static com.scholastic.torque.common.Robot.clickUsingJavaScript;
import static com.scholastic.torque.common.WaitUtils.waitForDisplayed;
import static com.scholastic.torque.common.WrapperFunctions.clickUsingActions;
import static com.scholastic.torque.common.WrapperFunctions.waitUsingThreadSleep;
import static org.testng.Assert.assertEquals;

public class ecomm_steps {

    public static ecomm_page page = new ecomm_page() {
        @Override
        protected void openPage() {

        }
    };
    public static cpq_Page page1 = new cpq_Page();
    public static String ecommorderno;

    @Given("^user lanuch the Teachables site$")
    public void user_lanuch_the_teachables_site() throws Throwable {
        page.openTeachablesPage();
        Thread.sleep(2000);
    }
    @Given("^user lanuch the MyScholastic site$")
    public void user_lanuch_the_myscholastic_site() throws Throwable {
        page.openMyScholasticPage();
    }

    @And("^user clicks on register link in myScholastic$")
    public void user_clicks_on_register_link_in_myscholastic() throws Throwable {
        Actions action = new Actions(TestBaseProvider.getTestBase().getDriver());
        WebElement we = TestBaseProvider.getTestBase().getDriver().findElement(By.xpath("//*[@id=\"contentcolumn\"]/div[1]/div[3]/div/div[1]/div[5]/div/div/div[2]/div/div[1]/div[1]"));
        action.moveToElement(we).click().build().perform();
        Thread.sleep(1000);
        TestBaseProvider.getTestBase().getDriver().findElement(By.xpath("//a[@class='sch-global-register']")).click();
//        page.getMyScholasticregister().click();
    }
    @Then("^user closes the TCB Welcome Popup")
    public void user_closes_the_TCB_Welcome_Popup() throws InterruptedException {
        waitForDisplayed(page.getWelcomePopupClose());
        page.getWelcomePopupClose().click();
        Thread.sleep(1000);
        page.getWelcomePopupCloseScreen2().click();
        Thread.sleep(1000);
    }

    @When("^user clicks on register image$")
    public void user_clicks_on_register_image() throws Throwable {
        CommonUtil.focusWebElement(page.getMyScholasticProfileicon());
        CommonUtil.hoverElement(page.getMyScholasticProfileicon(),5);
        page.getMyScholasticregister().click();
    }

    @And("^user enters valid credentials to register in registration screen$")
    public void user_enters_valid_credentials_to_register_in_registration_screen() throws Throwable {
        page.enter_valid_credentials_in_registration_screen();
    }

    @And("^user clicks on role drop down to select a role$")
    public void user_clicks_on_role_drop_down_to_select_a_role() throws Throwable {
        clickUsingJavaScript (page.getRegisterSelectRoleDropdown());
        clickUsingJavaScript(page.getRegisterRegisterSelectRoleDropdownOptions().get(Integer.parseInt(TestDataUtils.getString("RoleIndex"))));
        Thread.sleep(1000);
    }

    @And("^user selects an school type  on school selection screen$")
    public void user_selects_an_school_type_on_school_selection_screen() throws Throwable {
        Thread.sleep(1000);
        clickUsingJavaScript(page.getSchoolTypeSelectionRadioButtons().get(Integer.parseInt(TestDataUtils.getString("SchoolIndex"))));
        Thread.sleep(1000);
        clickUsingJavaScript(page.getRegisterNextButton());
        Thread.sleep(1000);
    }

    @And("^user enters zipcode$")
    public void user_enters_zipcode() throws Throwable {
        page.getRegisterSchoolZipcode().sendKeys(TestDataUtils.getString("regZipcode"));
    }

    @And("^user selects single grade on role selection screen$")
    public void user_selects_single_grade_on_role_selection_screen() throws Throwable {
        Thread.sleep(1000);
        clickUsingJavaScript(page.getRegisterTeacherGradeDropdown().get(0));
        Thread.sleep(1000);
        clickUsingJavaScript(page.getRegisterTeacherGradeDropdownOptions().get(5));
        Thread.sleep(1000);
        clickUsingJavaScript(page.getRegisterNextButton());
        Thread.sleep(1000);
    }

    @When("^user Navigates to oracle Bigmachine home page$")
    public void user_navigates_to_oracle_bigmachine_home_page() throws Throwable {
        TestBaseProvider.getTestBase().getDriver().get(TestDataUtils.getString("OracleBigMachineURL"));
    }

    @And("^user clicks on credit card paymentmode$")
    public void user_clicks_on_credit_card_paymentmode() throws Throwable {
        throw new PendingException();
    }

    @Then("^user clicks on Start Now CTA$")
    public void user_clicks_on_start_now_cta() throws Throwable {
        Thread.sleep(1000);
        clickUsingActions(page.gettermscheckbox(),"Click on Checkbox");
        waitForDisplayed(page.getSubmitbutton());
        clickUsingActions(page.getSubmitbutton(),"Click on Submit button");
        Thread.sleep(3000);
        ecommorderno = page.Submitordernumber();
        System.out.println("Ecomm order no "+ecommorderno);
        Thread.sleep(1300000);
    }

    @Then("^user clicks on Start Now CTA for CC$")
    public void user_clicks_on_start_now_cta_for_cc() throws Throwable {
        Thread.sleep(1000);
        waitForDisplayed(page.getContinuebtn());
        clickUsingJavaScript(page.getContinuebtn());
        Thread.sleep(1000);
        CommonUtil.focusWebElement(page.gettermscheckbox());
        Thread.sleep(1000);
        clickUsingActions(page.gettermscheckbox(),"Click on Checkbox");
        Thread.sleep(1000);
        waitForDisplayed(page.getSubmitbutton());
        clickUsingActions(page.getSubmitbutton(),"Click on Submit button");
        Thread.sleep(3000);
        page.Submitordernumber();
        System.out.println("Ecomm order no "+ecommorderno);
        Thread.sleep(20000);
    }

    @And("^user enters school data$")
    public void user_enters_school_data() throws Throwable {
        clickUsingJavaScript(page.getRegisterSelectSchoolDropdown());
        Thread.sleep(1000);
        clickUsingJavaScript(page.getRegisterSelectSchoolDropdownOptions().get(Integer.parseInt(TestDataUtils.getString("SchoolName"))));
        clickUsingJavaScript(page.getRegisterNextButton());
        Thread.sleep(1000);
    }

    @And("^user is on registration complete screen for TCB teacher$")
    public void user_is_on_registration_complete_screen_for_tcb_teacher() throws Throwable {
        page.validate_registration_complete_screen_for_TCB_teacher();
        Thread.sleep(20000);
    }

    @And("^user clicks on POpaymentmode$")
    public void user_clicks_on_popaymentmode() throws Throwable {
        page.completePOOrder();
    }

    @When("^User Search for New account created in Myscholastic$")
    public void user_search_for_new_account_created_in_myscholastic() throws Throwable {
        waitForDisplayed(page.getMyAccountSearchFirstName());
        page.getMyAccountSearchFirstName().sendKeys(page.getSearchFirstname());
        page.getMyAccountSearchLastName().sendKeys(page.getSearchLastname());
        Thread.sleep(2000);
        page1.getCPQ_SearchAccount_btn().click();
        Thread.sleep(2000);
        page1.gettblaccountsSearch().click();
    }

    @Then("^Verify the SearchAccount is filtered$")
    public void verify_the_searchaccount_is_filtered() throws Throwable {
        Thread.sleep(2000);
        waitForDisplayed(page.getShipToFirstName());
        if (page.getShipToFirstName().getText()==TestDataUtils.getString("regFirstname")+" "+TestDataUtils.getString("regLastname") && page.getShipToFirstName().getText()==TestDataUtils.getString("regFirstname")+" "+TestDataUtils.getString("regLastname") && page.getSoldToFirstName().getText()==TestDataUtils.getString("regFirstname")+" "+TestDataUtils.getString("regLastname")){
            assertEquals(page.getBillToFirstName().getText(),TestDataUtils.getString("regFirstname")+" "+TestDataUtils.getString("regLastname"));
        }
    }

    @And("^System should display search account information in Quote page$")
    public void system_should_display_search_account_information_in_quote_page() throws Throwable {
        throw new PendingException();
    }

    @And("^user navigates to subscription page \"([^\"]*)\"$")
    public void userNavigatesToSubscriptionPage(String arg0) throws Throwable {
        TestBaseProvider.getTestBase().getDriver().get(arg0);
        Thread.sleep(5000);
    }

    @And("^user clicks on CCpaymentmode$")
    public void user_clicks_on_CCpaymentmode() throws Throwable {
    	 waitForDisplayed(page.getCCradiobtn());
        page.getCCradiobtn().click();
    }

    @And("^User enter all credit card information like \"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\"$")
    public void user_enter_all_credit_card_information_like_somethingsomethingsomethingsomethingsomethingsomethingsomethingsomethingsomethingsomethingsomethingsomething(String firstname, String lastname, String firstaddress, String secondaddress, String city, String state, String zipcode, String phone, String ccnumber, String ccmonth, String ccyear, String ccvn) throws Throwable {
        page.getecomm_cc_firstname().sendKeys(firstname);
        page.getecomm_cc_Lastname().sendKeys(lastname);
        page.getecomm_cc_Addr1().sendKeys(firstaddress);
        page.getecomm_cc_Addr2().sendKeys(secondaddress);
        Thread.sleep(200);
        page.getecomm_cc_City().sendKeys(city);
        Thread.sleep(200);
        CommonUtil.selectFromDropdownByVisibleText(page.getecomm_cc_State(),state);
        page.getecomm_cc_Zip().sendKeys(zipcode);
        page.getecomm_cc_Phone().sendKeys(phone);
        page.getecomm_cc_cnumber().sendKeys(ccnumber);
        CommonUtil.selectFromDropdownByVisibleText(page.getecomm_cc_Month(),ccmonth);
        CommonUtil.selectFromDropdownByVisibleText(page.getecomm_cc_Year(),ccyear);
        page.getecomm_cc_cnumber().sendKeys(ccnumber);
        Thread.sleep(200);
        page.getecomm_cc_cvn().sendKeys(ccvn);
        page.getecomm_cc_cnumber().sendKeys(Keys.TAB);
        Thread.sleep(200);
        page.getecomm_cc_cnumber().sendKeys(Keys.SPACE);
        Thread.sleep(200);
        page.getccSavebutton().click();
        Thread.sleep(200);
        page.getContinuebtn().click();
    }

}

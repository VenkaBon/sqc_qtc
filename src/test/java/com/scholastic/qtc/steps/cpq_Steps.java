package com.scholastic.qtc.steps;

import com.scholastic.qtc.pages.cpq_Page;
import com.scholastic.qtc.pages.ecomm_page;
import com.scholastic.qtc.support.CommonUtil;
import com.scholastic.torque.common.BaseTestPage;
import com.scholastic.torque.common.TestBase;
import com.scholastic.torque.common.TestBaseProvider;
import com.scholastic.torque.common.WaitUtils;
import com.scholastic.torque.common.WrapperFunctions;
import com.torque.automation.core.TestDataUtils;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.math.RandomUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import java.io.File;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import static com.scholastic.qtc.steps.ecomm_steps.ecommorderno;
import static com.scholastic.qtc.support.CommonUtil.waitTillDivisionLoads;
import static com.scholastic.torque.common.Robot.clickUsingJavaScript;
import static com.scholastic.torque.common.WaitUtils.waitForDisplayed;
import static com.scholastic.torque.common.WaitUtils.waitForElementToBeClickable;
import static com.scholastic.torque.common.WrapperFunctions.switchToNewWindow;
import static org.testng.Assert.*;
import static com.scholastic.qtc.support.HelperClass.WaitingThread;
import static com.scholastic.qtc.support.HelperClass.WakingThread;

public class cpq_Steps {
    private cpq_Page page = new cpq_Page();
    private ecomm_page page1 = new ecomm_page() {
        @Override
        protected void openPage() {

        }
    };
    Thread waitThread = new WaitingThread();
    Thread wakingThread = new WakingThread();
    Logger logger= LogManager.getLogger(cpq_Steps.class);
    public static String ordrefno;
    public static String cpqordernumber;
    public String PONumber;
    public String ISBNSelected;
    @Given("^user opens oracle Bigmachine home page$")
    public void user_logs_into_the_cpqapplication() throws Throwable {
        page.openCPQPage();
    }
    @When("^User enters valid credentails to login$")
    public void user_enters_valid_credentails_to_login() throws Throwable {
        page.LoginToCPQ();
    }
    @Then("^user verify the Oracle Quote to Order - Manager present under Home Page$")
    public void user_verify_the_something_present_under_home_page() {
        if(page.OrcQuoteTOOrderManager.isDisplayed()) {
            assertTrue(page.OrcQuoteTOOrderManager.isDisplayed());
            logger.info("Log.INFO::Oracle Quote to Order - Manager is present under Home Page");
        }else{
            logger.info("Log.INFO::Oracle Quote to Order - Manager is not present under Home Page");
        }
    }
    @When("^User clicks on Quote to Order - Manager link$")
    public void User_clicks_on_Quote_to_Order_Manager_link (){
        waitForDisplayed(page.OrcQuoteTOOrderManager);
        if(page.OrcQuoteTOOrderManager.isDisplayed()) {
            page.OrcQuoteTOOrderManager.click();
        }else{
            logger.info("Log.INFO::Oracle Quote to Order - Manager is not able to clickable");
        }
    }

    @Then("^User verify the Quote to Order - Manager page displayed$")
    public void User_verify_the_QuotetoOrderManager_page_displayed() throws Throwable {
//        waitTillDivisionLoads();
    	Thread.sleep(3000);
        waitForDisplayed(page.HdingOrcQuoteToOrderManager);
        if(page.HdingOrcQuoteToOrderManager.isDisplayed()) {
            assertTrue(page.HdingOrcQuoteToOrderManager.isDisplayed());
            logger.info("Log.INFO::Oracle Quote to Order - Manager page is displayed on click on Quote to Order Manager link");
        }else{
            logger.info("Log.INFO::Oracle Quote to Order - Manager page is not displayed on click on Quote to Order Manager link");
        }
    }

    @When("^User click on New Transaction button$")
    public void user_click_on_new_transaction_button() throws Throwable {
        waitForDisplayed(page.getCPQ_NewTransactionbtn());
        if(page.getCPQ_NewTransactionbtn().isDisplayed()){
        page.getCPQ_NewTransactionbtn().click();
        logger.info("Log.INFO::User clicks on New Transaction button");}
        else{
            logger.info("Log.INFO::User unable to clicks on New Transaction button");
        }
    }

    @When("^User click on selectAccount button$")
    public void user_click_on_selectaccount_button() throws Throwable {
        waitForElementToBeClickable(page.getSelectAccount());
        if(page.getSelectAccount().isDisplayed()){
            page.getSelectAccount().click();
            logger.info("Log.INFO::User clicked on SelectAccount button");
        }else{
            logger.info("Log.INFO::User unable to clicked on SelectAccount button");
        }
    }

    @Then("^user verify the system lands on the Quote page$")
    public void user_verify_the_system_lands_on_the_quote_page() throws Throwable {
        if(page.HdingQuote.isDisplayed()) {
            assertTrue(page.HdingQuote.isDisplayed());
            logger.info("Log.INFO::Quote page is displayed on click on New Transaction");
        }else{
            logger.info("Log.INFO::Quote page is not displayed on click on New Transaction");
        }
    }

    @When("^User search for \"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\" account$")
    public void user_search_for_somethingsomethingsomethingsomething_account(String firstname, String lastname, String email, String unc) throws Throwable {
        waitForDisplayed(page.getMyAccountSearchFirstName());
        page.getMyAccountSearchFirstName().sendKeys(TestDataUtils.getString("Firstname"));
        Thread.sleep(200);
        page.getMyAccountSearchLastName().sendKeys(TestDataUtils.getString("Lastname"));
        Thread.sleep(200);
//        page.getMyAccountSearchEmail().sendKeys(TestDataUtils.getString("Email"));
//        Thread.sleep(200);
//        page.getMyAccountSearchUNC().sendKeys(TestDataUtils.getString("unc"));
        Thread.sleep(2000);
        page.getCPQ_SearchAccount_btn().click();
        Thread.sleep(2000);
        page.gettblaccountsSearch().click();
    }

    @When("^User click on Quote Order Details link$")
    public void user_click_on_quote_order_details_link() throws Throwable {
        waitForDisplayed(page.OrderDetails);
        if(page.OrderDetails.isDisplayed()) {
            page.OrderDetails.click();
        }else{
            logger.info("Log.INFO::Quote/Order Details link is not able to clickable");
        }
    }

    @When("^User click on add_line_item button$")
    public void user_click_on_addlineitem_button() throws Throwable {
        Thread.sleep(4);
        waitForDisplayed(page.getCPQ_AddLineItembtn());
        if(page.getCPQ_AddLineItembtn().isDisplayed()){
            page.getCPQ_AddLineItembtn().click();
            logger.info("Log.INFO::User Able to click on Add Line item button");
        }else{
            logger.info("Log.INFO::User un-able to click on Add Line item button");
        }
    }

    @When("^User click on Teachables Subscriptions button$")
    public void user_click_on_teachables_subscriptions_button() throws Throwable {
        page.hoverElement(page.geteducationteachables(),2);
        Thread.sleep(300);
        clickUsingJavaScript(page.getCPQ_Teachables_Subscription_btn());
//        waitForDisplayed(page.getCPQ_Teachables_Subscription_btn());
//        if(page.getCPQ_Teachables_Subscription_btn().isDisplayed()){
//            page.getCPQ_Teachables_Subscription_btn().click();
//            logger.info("Log.INFO::User able to click on Teachables subscription button");
//        }else{
//            logger.info("Log.INFO::User un-able to click on Teachables subscription button");
//        }
    }

    @When("^user select value \"([^\"]*)\" from oLDISBN drop down$")
    public void user_select_value_something_from_oldisbn_drop_down(String oldisbncode) throws Throwable {
        Select ISBN = new Select(page.getSelectDDOldISBN());
        ISBN.selectByIndex((int) Math.floor((Math.random() * 10) + 1));
        
        ISBNSelected = page.getSelectDDOldISBN().getAttribute("data-initial-value");
        System.out.println("Selected ISBN: "+ISBNSelected);
        
//        ISBN.selectByValue(TestDataUtils.getString("OldISBNCode"));
    }

    @When("^User click on save button$")
    public void user_click_on_save_button() throws Throwable {
        waitForDisplayed(page.getCPQ_Save_btn());
        if(page.getCPQ_Save_btn().isDisplayed()){
            page.getCPQ_Save_btn().click();
            logger.info("Log.INFO::User able to click save button");
        }else{
            logger.info("Log.INFO::User unable to click save button");
        }
    }

    @When("^User click on submit_for_approval button$")
    public void user_click_on_submitforapproval_button() throws Throwable {
//        page.getCPQ_submitForApproval_btn().click();
//        waitForDisplayed(page.getCPQ_submitForApproval_btn());
        if(page.btnSubmitApproval.isDisplayed()){
            page.btnSubmitApproval.click();
            logger.info("Log.INFO::User able to click Submit for approval button");
        }else{
            logger.info("Log.INFO::User unable to click Submit for approval button");
        }
    }

    @When("^User click on validation_check button$")
    public void user_click_on_validationcheck_button() throws Throwable {
        waitForDisplayed(page.getCPQ_ValidationCheck());
        if(page.getCPQ_ValidationCheck().isDisplayed()){
            page.getCPQ_ValidationCheck().click();
            logger.info("Log.INFO::User able to click Validation Check button");
        }else{
            logger.info("Log.INFO::User unable to click Validation Check button");
        }
    }

    @When("^User click on cancel_approvalsrevise button$")
    public void user_click_on_cancelapprovalsrevise_button() throws Throwable {
        waitForDisplayed(page.getCPQ_CancelApprovalRevise_btn());
        if(page.getCPQ_CancelApprovalRevise_btn().isDisplayed()){
            page.getCPQ_CancelApprovalRevise_btn().click();
            logger.info("Log.INFO::User able to click Cancel Approval revise button");
        }else{
            logger.info("Log.INFO::User able to click Cancel Approval revise button");
        }
    }

//    @When("^User clicks on Quote to Order - Manager link$")
//    public void user_clicks_on_quote_to_order_manager_link() throws Throwable {
//        waitForDisplayed(page.OrcQuoteTOOrderManager);
//        if(page.OrcQuoteTOOrderManager.isDisplayed()) {
//            page.OrcQuoteTOOrderManager.click();
//        }else{
//            logger.info("Log.INFO::Oracle Quote to Order - Manager is not able to clickable");
//        }
//    }

    @When("^User logged in the OSS system with valid credentials$")
    public void user_logged_in_the_oss_system_with_valid_credentials() throws Throwable {
        TestBaseProvider.getTestBase().getDriver().get(TestDataUtils.getString("OracleFusionURL"));
        Thread.sleep(5000);
        waitForDisplayed(page.getOSS_UserName());
        System.out.println("Username:"+TestDataUtils.getString("OSSUsername"));
        page.getOSS_UserName().sendKeys(TestDataUtils.getString("OSSUsername"));
        page.getOSS_password().sendKeys(TestDataUtils.getString("OSSPassword"));
        page.getOSS_LoginBtn().click();
    }

    @Then("^System should display the Select Account search page$")
    public void system_should_display_the_select_account_search_page() throws Throwable {
        waitForDisplayed(page.getMyAccountSearchFirstName());
        if(page.getMyAccountSearchFirstName().isDisplayed()) {
            assertTrue(page.getMyAccountSearchFirstName().isDisplayed());
            logger.info("Log.INFO::scholasticMyAccountSearch is displayed");
        }else{
            logger.info("Log.INFO::scholasticMyAccountSearch is not displayed");
        }
    }

    @Then("^Verify the SearchAccount \"([^\"]*)\" is filtered$")
    public void verify_the_searchaccount_something_is_filtered(String unc) throws Throwable {
        System.out.println(page.gettblaccountsSearch().getText());
        page.gettblaccountsSearch().getText().contains(TestDataUtils.getString("Firstname"));
        page.gettblaccountsSearch().click();

    }

    @Then("^verify the Order Status \"([^\"]*)\" under the Order details$")
    public void verify_the_order_status_something_under_the_order_details(String strArg1) throws Throwable {
        String sts=page.getOrdStatus().getText();
        System.out.println("System sts is"+sts+"Expected sts is "+strArg1);
        Assert.assertEquals(sts,strArg1);
    }

    @Then("^System should display the Teaching Resources page$")
    public void system_should_display_the_teaching_resources_page() throws Throwable {
        waitForDisplayed(page.getHdingTeachingResources());
        if(page.getHdingTeachingResources().isDisplayed()) {
            assertTrue(page.getHdingTeachingResources().isDisplayed());
            logger.info("Log.INFO::Teaching Resources page is displayed");
        }else{
            logger.info("Log.INFO::Teaching Resources page is not displayed");
        }
    }

    @Then("^System should display Model Configuration page$")
    public void system_should_display_model_configuration_page() throws Throwable {
        waitForDisplayed(page.gethdingModelConfiguration());
        if(page.gethdingModelConfiguration().isDisplayed()) {
            assertTrue(page.gethdingModelConfiguration().isDisplayed());
            logger.info("Log.INFO::Model Configuration page is displayed");
        }else{
            logger.info("Log.INFO::Model Configuration page is not displayed");
        }
    }

    @Then("^System should display the selected subscription \"([^\"]*)\" under line items$")
    public void system_should_display_the_selected_subscription_something_under_line_items(String strSearchText)  {
        System.out.println(page.lineItemadded.getText()+"----------"+TestDataUtils.getString("OldISBNCode"));
        assertEquals(page.lineItemadded.getText(),ISBNSelected);
        logger.info(TestDataUtils.getString("OldISBNCode") + " is present in Line item table");
    }

    @Then("^User should able to see the New Quote number$")
    public void user_should_able_to_see_the_new_quote_number() throws Throwable {
        ordrefno = page.OrdRefNo.getText();
        logger.info("Log INFO:: New quote number :"+ordrefno);
    }

    @Then("^System should display \"([^\"]*)\"$")
    public void system_should_display_something(String strArg1) throws Throwable {
        waitForDisplayed(page.getCPQ_HoldMessage_stm());
        Assert.assertTrue(page.getCPQ_HoldMessage_stm().isDisplayed());
    }

    @Then("^User should able to see the newly added quote in Oracle Quote to order page$")
    public void user_should_able_to_see_the_newly_added_quote_in_oracle_quote_to_order_page() throws Throwable {
        System.out.println("table content::"+page.tblCMManagerContent.getText());
//        Assert.assertTrue(page.tblCMManagerContent.getText().contains(ordrefno));
        CommonUtil.verifyDataInTableColumns(page.tblCMManagerContent,"Quote/Order Number",ordrefno);

    }

    @Then("^System should able to retrive the search subscription details successfully$")
    public void system_should_able_to_retrive_the_search_subscription_details_successfully() throws Throwable {
        Thread.sleep(3000 );
        System.out.println(page.OssSearchResultSubscriptionid.getText()+"--------------"+ordrefno);
        Assert.assertTrue(page.OssSearchResultSubscriptionid.getText().contains(ordrefno));
    }

    @And("^User click on searchAccount button$")
    public void user_click_on_searchaccount_button() throws Throwable {
        Thread.sleep(2000);
        page.getCPQ_SearchAccount_btn().click();
        Thread.sleep(2000);
    }

    @And("^System should display selected account information below \"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\"$")
    public void system_should_display_selected_account_information_below_somethingsomethingsomething(String firstname, String lastname, String email) throws Throwable {
        Thread.sleep(2000);
        if (page.BillToFirstName.getText()==firstname+" "+lastname && page.ShipToFirstName.getText()==firstname+" "+lastname && page.SoldToFirstName.getText()==firstname+" "+lastname){
            assertEquals(page.BillToFirstName.getText(),firstname+" "+lastname);
            logger.info("Log.INFO::Selected account is displayed");
        }else{
            logger.info("Log.INFO::Selected account is not displayed");
        }
    }

    @And("^QuoteOrdernumber is Autopopulated$")
    public void quoteordernumber_is_autopopulated() throws Throwable {
        assertTrue(page.QuoteOrderNumber.getText().contains("CPQ-"));
        cpqordernumber = page.QuoteOrderNumber.getText();
    }

    @And("^QuoteType is \"([^\"]*)\"$")
    public void quotetype_is_something(String strArg1) throws Throwable {
        assertTrue(page.QuoteType.getText().contains("New"));
    }
    @And("^Select the Order Payment terms as \"([^\"]*)\"$")
    public void select_the_order_payment_terms_as_something(String paymentmode) throws Throwable {
    	int sr = 0;
    	switch(paymentmode) {
    	  case "Credit Card":
    		  sr=2;
    	    break;
    	  case "Credit":
    		  sr=1;
    	    break;
    	  case "Wire":
    		  sr=0;
    	    break;
    	  case "PO":
    		  sr=3;
    	    break;
    	  default:
    	    // code block
    	}
        Select pppay = new Select(page.QuotePaymentMethod);
        pppay.selectByIndex(sr);
//        CommonUtil.selectFromDropdownByVisibleText(page.QuotePaymentMethod,paymentmode);
    }
	@And("^Business Unit is \"([^\"]*)\"$")
    public void business_unit_is_something(String strArg1) throws Throwable {
        assertTrue(page.QuoteBusinessUnit.getText().contains("US Business Unit"));
    }

    @And("^QuoteExpiry Date is autopopulated$")
    public void quoteexpiry_date_is_autopopulated() throws Throwable {
        WebElement dateBox = page.QuoteExpiryDate;
        Assert.assertTrue(dateBox.getAttribute("value").contains("2020"));
    }
    @And("^Select the Order Payment terms as PO$")
    public void select_the_order_payment_terms_as_po() throws Throwable {
        page.QuotePaymentMethod.sendKeys("PO");
        Thread.sleep(2000);
        page.QuotePONumber.sendKeys(RandomStringUtils.randomNumeric(9));
        Thread.sleep(2000);
        PONumber = page.QuotePONumber.getText();
        System.out.println(PONumber);
    }

    @And("^Select the Order Payment terms as Creditcard$")
    public void select_the_order_payment_terms_as_cc() throws Throwable {
        Select ddrop = new Select(page.QuotePaymentMethod);
        ddrop.selectByValue("Credit");
    }
    @When("^And Select the Order Payment terms as \"([^\"]*)\"$")
    public void and_select_the_order_payment_terms_as_something(String paymentmode) throws Throwable {
        Select ddrop = new Select(page.QuotePaymentMethod);
        ddrop.selectByValue(paymentmode);
    }

    @And("^User click on add_to_transaction button$")
    public void user_click_on_addtotransaction_button() throws Throwable {
        Thread.sleep(3000);
        page.getCPQ_addToTransaction_btn().click();
        Thread.sleep(3000);
    }

    @And("^User should also able to see the Submit for Approval button$")
    public void user_should_also_able_to_see_the_submit_for_approval_button() throws Throwable {
        assertTrue(page.btnSubmitApproval.isDisplayed(),"Submit for Approval button is enabled");
    }

    @And("^User click on Hold Details link$")
    public void user_click_on_hold_details_link() throws Throwable {
        page.lnkHolddetails.click();
        Thread.sleep(2000);
    }

    @And("^User Selects the Checkboxes and Enter Release reason code and release comments$")
    public void user_selects_the_checkboxes_and_enter_release_reason_code_and_release_comments() throws Throwable {
        Thread.sleep(3000);
        if (page.rdChbxReleaseHold.isDisplayed()) {
            page.rdChbxReleaseHold.click();
            Select ddrop = new Select(page.ddropReleasecreditcheck);
            ddrop.selectByValue("Pass Credit");
            page.editReasoncreditcheck.sendKeys("Testing credit check");
        }
        if (page.rdChbxReleaseduphold.isDisplayed()){
            page.rdChbxReleaseduphold.click();
            Select ddrop = new Select(page.ddropReleasecreditcheck);
            ddrop.selectByValue("Other");
            page.editReasoncreditcheck.sendKeys("Testing Other check");
        }
    }

    @And("^User click on release_holds button$")
    public void user_click_on_releaseholds_button() throws Throwable {
        waitForDisplayed(page.getCPQReleaseHoldbtn());
        if(page.getCPQReleaseHoldbtn().isDisplayed()){
            page.getCPQReleaseHoldbtn().click();
            logger.info("Log.INFO::User able to click Release Hold button");
        }else{
            logger.info("Log.INFO::User un able to click Release Hold button");
        }
    }

//    @And("^User click on submit_for_approval button$")
//    public void user_click_on_submit_for_approval_button() throws Throwable {
//        waitForDisplayed(page.getCPQ_submitForApproval_btn());
//        if(page.getCPQ_submitForApproval_btn().isDisplayed()){
//            page.getCPQ_submitForApproval_btn().click();
//            logger.info("Log.INFO::User able to click Submit for approval button");
//        }else{
//            logger.info("Log.INFO::User un able to click Submit for approval button");
//        }
//    }

//    @And("^User click on validation_check button$")
//    public void user_click_on_validation_check_button() throws Throwable {
//        waitForDisplayed(page.getCPQ_submitForApproval_btn());
//        if(page.getCPQ_submitForApproval_btn().isDisplayed()){
//            page.getCPQ_submitForApproval_btn().click();
//            logger.info("Log.INFO::User able to click Submit for approval button");
//        }else{
//            logger.info("Log.INFO::User un able to click Submit for approval button");
//        }
//    }

    @And("^User click on submit_order button$")
    public void user_click_on_submitorder_button() throws Throwable {
        waitForDisplayed(page.getCPQSubmitOrderbtn());
        if(page.getCPQSubmitOrderbtn().isDisplayed()){
            page.getCPQSubmitOrderbtn().click();
            logger.info("Log.INFO::User able to click Submit order button");
        }else{
            logger.info("Log.INFO::User un able to click Submit order button");
        }
    }

    @And("^User click on QuoteOrder Details link$")
    public void user_click_on_quoteOrder_details_link() throws Throwable {
        waitForDisplayed(page.OrderDetails);
        if(page.OrderDetails.isDisplayed()) {
            page.OrderDetails.click();
        }else{
            logger.info("Log.INFO::Quote/Order Details link is not able to clickable");
        }
    }

    @And("^User clicks logout$")
    public void user_clicks_logout() throws Throwable {
        page.getCPQ_Logout().click();
        Thread.sleep(3000);
    }

    @And("^User click on btnActive button$")
    public void user_click_on_btnactive_button() throws Throwable {

    }

    @Then("^System should display Subscription Management on home page$")
    public void system_should_display_subscription_management_on_home_page() throws Throwable {
        Thread.sleep(4000);
        waitForDisplayed(page.getOSS_SubscriptionManagementBtn());
        Assert.assertTrue(page.getOSS_SubscriptionManagementBtn().isDisplayed());
    }

    @And("^User click on itemNode_SubscriptionManagement_SubscriptionManagement button$")
    public void user_click_on_itemnodesubscriptionmanagementsubscriptionmanagement_button() throws Throwable {
        Thread.sleep(2000);
       waitForDisplayed(page.getOSS_SubscriptionManagementBtn());
       page.getOSS_SubscriptionManagementBtn().click();
        Thread.sleep(2000);
    }

    @And("^User search for the Subscription ID which is created in CPQ and flew in OSS$")
    public void user_search_for_the_subscription_id_which_is_created_in_cpq_and_flew_in_oss() throws Throwable {
        Thread.sleep(4000);
        waitForDisplayed(page.OsseditSearchSubscriptionID);
        page.OsseditSearchSubscriptionID.sendKeys(ordrefno);
        Thread.sleep(2000);
        page.OsseditSearchSubscriptionID.click();
        page.OsseditSearchSubscriptionID.sendKeys(Keys.RETURN);
        Thread.sleep(2000);
    }

    @And("^User search for the Subscription ID which is created in ecomm and flew through CPQ in OSS$")
    public void user_search_for_the_subscription_id_which_is_created_in_ecomm_and_flew_through_cpq_in_oss() throws Throwable {
        Thread.sleep(4000);
        waitForDisplayed(page.OsseditSearchSubscriptionID);
        page.OsseditSearchSubscriptionID.sendKeys(ecommorderno);
        Thread.sleep(2000);
        page.OsseditSearchSubscriptionID.click();
        page.OsseditSearchSubscriptionID.sendKeys(Keys.RETURN);
        Thread.sleep(2000);
    }
    
    @And("^User search for the Subscription ID which is created and closed in CPQ$")
    public void user_search_for_the_subscription_id_which_is_created_and_closed_in_cpq() throws Throwable {
        Thread.sleep(4000);
        System.out.println("Order checking in COQ:"+ordrefno);
        Thread.sleep(2000);
        page.OsseditSearchSubscriptionID.sendKeys(ordrefno);
        Thread.sleep(2000);
        waitForDisplayed(page.OsseditSearchSubscriptionID);
        page.OsseditSearchSubscriptionID.click();
        page.OsseditSearchSubscriptionID.sendKeys(Keys.RETURN);
        Thread.sleep(2000);
    }

    @And("^User click on search button$")
    public void user_click_on_search_button() throws Throwable {
        
        waitThread.start();
        wakingThread.start();
        Thread.sleep(2000);
        waitThread.join();
        wakingThread.join();
        page.getCPQ_Search_btn().click();
    }

    @And("^Search for order source number submitted in teachables$")
    public void search_for_order_source_number_submitted_in_teachables() throws Throwable {
    	TimeUnit.MINUTES.sleep(30);
        CommonUtil.switchToSecondWindow();
        Thread.sleep(2000);
        page.getCPQSearchpgorderSourceno().click();
        page.getCPQSearchpgnextbtn().click();
        Thread.sleep(2000);
        page.getCPQSearchpgdisplaylistorderSourceno().click();
        page.getCPQSearchpgviewsearcheditnextbtn().click();
        CommonUtil.selectFromDropdownByVisibleText(page.getCPQSearchpgComparator(), "=");
//        		Select Comparator = new Select(page.getCPQSearchpgComparator());
        Thread.sleep(1000);
//        Comparator.selectByValue("=");

//        page.getCPQSearchpgComparatorvalue().sendKeys("80028963)");
        page.getCPQSearchpgComparatorvalue().sendKeys(page1.Submitordernumber());
        page.getCPQSearchpgsearchbtn().click();
        waitTillDivisionLoads();
        CommonUtil.switchToFrame();
        clickUsingJavaScript(TestBaseProvider.getTestBase().getDriver().findElement(By.xpath("//a[@class='list-field']")));
        waitTillDivisionLoads();
        CommonUtil.switchToFirstWindow();
        if(page.HdingQuote.isDisplayed()) {
            assertTrue(page.HdingQuote.isDisplayed());
            logger.info("Quote page is displayed on click on New Transaction");
        }else{
            logger.info("Quote page is not displayed on click on New Transaction");
        }
    }

//    @When ("^User click on QuoteOrder Details link$")
//    public void user_click_on_quoteorder_details_link() throws Throwable {
//        waitForDisplayed(page.OrderDetails);
//        if(page.OrderDetails.isDisplayed()) {
//            page.OrderDetails.click();
//        }else{
//            logger.info("Quote/Order Details link is not able to clickable");
//        }
//    }

    @And("^user verify the search order details populated in quote page$")
    public void user_verify_the_search_order_details_populated_in_quote_page() throws Throwable {
        Assert.assertEquals(page.getCPQ_QuoteDetailspg_OrdSourceNo().getText(),page1.Submitordernumber());
    }

    @And("^user capture the Quote number$")
    public void user_capture_the_quote_number() throws Throwable {
        ordrefno = page.OrdRefNo.getText();
    }

}

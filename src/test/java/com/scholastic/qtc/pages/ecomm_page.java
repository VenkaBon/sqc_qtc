package com.scholastic.qtc.pages;

import com.scholastic.qtc.locators.ecomm_loc;
import com.scholastic.torque.common.TestBaseProvider;
import com.scholastic.torque.common.WaitUtils;
import com.torque.automation.core.TestDataUtils;
import gherkin.lexer.Th;
import org.apache.commons.lang.RandomStringUtils;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Wait;
import org.testng.Assert;

import java.util.List;

import static com.scholastic.torque.common.Robot.clickUsingJavaScript;
import static com.scholastic.torque.common.WaitUtils.waitForDisplayed;
import static com.scholastic.torque.common.WrapperFunctions.launchURL;
import static com.scholastic.torque.common.WrapperFunctions.waitUsingThreadSleep;

public abstract class ecomm_page extends ecomm_loc {
    public String ln = TestDataUtils.getString("regLastname")+ RandomStringUtils.randomAlphabetic(5);
    public static String email = new String();
    public String searchlastname = ln;
    public static String submittedorder;
    public static String POnumber;



    public void openMyScholasticPage() {
        launchURL(TestDataUtils.getString("MyScholasticURL"));
    }
    public void openTeachablesPage() {
        launchURL(TestDataUtils.getString("TeachablesURL"));
    }
    public void user_closes_the_TCB_Welcome_Popup() throws InterruptedException {
        Thread.sleep(6000);
        getWelcomePopupClose().click();
        Thread.sleep(1000);
        getWelcomePopupCloseScreen2().click();
        Thread.sleep(1000);
    }
    public void Clickteachablesregister() {
        gettechregisterlnk().click();
    }
    public void enter_valid_credentials_in_registration_screen() throws InterruptedException {
        TestBaseProvider.getTestBase().getDriver().switchTo().frame("GB_frame");
        Thread.sleep(500);
        waitForDisplayed(getRegisterNextButton());
        getRegisterScreenTextBox().get(0).sendKeys(TestDataUtils.getString("regFirstname"));
        Thread.sleep(500);
        getRegisterScreenTextBox().get(1).sendKeys(ln);
        Thread.sleep(500);
        String email = generateEmail();
        getRegisterScreenTextBox().get(2).sendKeys(email);
        Thread.sleep(2000);
        clickUsingJavaScript(getRegisterNextButton());
        Thread.sleep(2000);
        //password screen
        getRegisterCreatePasswordScreen().get(0).sendKeys(TestDataUtils.getString("regPassword"));
        Thread.sleep(500);
        getRegisterCreatePasswordScreen().get(1).sendKeys(TestDataUtils.getString("regPassword"));
        Thread.sleep(500);
        clickUsingJavaScript(getRegisterCreatePasswordScreenCheckBox().get(0));
        Thread.sleep(500);
        clickUsingJavaScript(getRegisterNextButton());
        Thread.sleep(500);
        System.out.println("********************");
    }
    public static String generateEmail() {
        email = "auto_email" + RandomStringUtils.randomAlphanumeric(4) + "@mailinator.com";
        System.out.println("email generated= "+email);
        return email;
    }

    public String Submitordernumber() {
        waitForDisplayed(getwebordnumber());
        submittedorder = getwebordnumber().getText();
        return submittedorder;
    }

    public void validate_registration_complete_screen_for_TCB_teacher() throws InterruptedException {
        Thread.sleep(3000);
        System.out.println("A: "+ getSuccessfullyRegisteredScreenNextCTA().getText().trim());
        System.out.println("B: "+ getSuccessfullyRegisteredScreenPreviousCTA().getText().trim());
        System.out.println("B: "+ getSuccessfullyRegisteredScreenPreviousCTA().getAttribute("data-myprofile-link").trim());
        System.out.println("C: "+ getSuccessfullyRegisteredScreenHeader().getText().trim());
        System.out.println("D: "+ getSuccessfullyRegisteredScreenHeaderDescription().getText().trim());
        Assert.assertTrue(getSuccessfullyRegisteredScreenNextCTA().getText().trim().equalsIgnoreCase("I'LL DO THIS LATER"));
        Assert.assertTrue(getSuccessfullyRegisteredScreenPreviousCTA().getText().trim().equalsIgnoreCase("GO TO MY PROFILE PAGE"));
        Assert.assertTrue(getSuccessfullyRegisteredScreenPreviousCTA().getAttribute("data-myprofile-link").trim().contains("/my-scholastic/profile/my-profile.html"));
        Assert.assertTrue(getSuccessfullyRegisteredScreenHeader().getText().trim().equalsIgnoreCase("Welcome to Scholastic!"));
        Assert.assertTrue(getSuccessfullyRegisteredScreenHeaderDescription().getText().trim().equalsIgnoreCase("You have completed your registration. Now, fill out your profile "
                + "information to take advantage of the benefits we offer registered teachers."));
        waitForDisplayed(getSuccessfullyRegisteredScreenNextCTA());
        getSuccessfullyRegisteredScreenNextCTA().click();
//        clickUsingJavaScript(getSuccessfullyRegisteredScreenNextCTA());
        Thread.sleep(20000);
        Thread.sleep(1000);
//        System.out.println("Expected::"+utils.properties.getProperty("FirstName")+" "+ln.toUpperCase()+":: Actual::"+loc1.getNameLeftNav().getText().trim());
//        Assert.assertTrue(loc1.getNameLeftNav().getText().trim().equalsIgnoreCase(utils.properties.getProperty("FirstName")+" "+ln.toUpperCase()));
    }

    public String completePOOrder() throws InterruptedException {
        getPOradiobtn().click();
        getPOnumber().sendKeys(RandomStringUtils.randomNumeric(6));
        Thread.sleep(1000);
        POnumber = getPOnumber().getText();
        Thread.sleep(200);
        getPOnumber().sendKeys(Keys.TAB);
        Thread.sleep(200);
        getContinuebtn().click();
        return POnumber;
    }

    public String completeCCOrder() throws InterruptedException {

        getCCradiobtn().click();
        getPOnumber().sendKeys(RandomStringUtils.randomNumeric(9));
        Thread.sleep(1000);
        POnumber = getPOnumber().getText();
        getPOnumber().sendKeys(Keys.TAB);
        Thread.sleep(1000);
        getContinuebtn().click();
        return POnumber;
    }

    public String getSearchFirstname() {
        return TestDataUtils.getString("regFirstname");
    }

    public String getSearchLastname() {
        return ln;
    }
    public String getSearchEmail() {
        return email;
    }

}

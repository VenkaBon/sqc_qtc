package com.scholastic.qtc.pages;

import com.scholastic.qtc.locators.DriverControl;
import com.scholastic.qtc.locators.cpq_Loc;
import com.scholastic.qtc.locators.oss_Loc;
import com.scholastic.torque.common.TestBaseProvider;
import com.scholastic.torque.common.WaitUtils;
import com.torque.automation.core.TestDataUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.io.IOException;

import static com.scholastic.torque.common.WaitUtils.waitForDisplayed;
import static com.scholastic.torque.common.WrapperFunctions.*;


public class cpq_Page extends cpq_Loc {


  Logger logger= LogManager.getLogger(cpq_Page.class);
  DriverControl propertyFile=new DriverControl();

	public void openPage() {
		System.out.println("Google");
		launchURL(TestDataUtils.getString("url"));
	}

	public void openCPQPage() {
		launchURL(TestDataUtils.getString("OracleBigMachineURL"));
	}
	public void wait(int time) throws InterruptedException {
		getDriver().wait(time);
	}
	public void verifyCPQLandingPage()
	{
		waitForDisplayed(OrcQuoteTOOrderManager);
		if(OrcQuoteTOOrderManager.isDisplayed())
		{
			logger.info("CPQ landed on Home Page");
		}
		else
		{
			Assert.fail();
		}
	}

	public void LoginToCPQ()
	{
		System.out.println("User name :: "+TestDataUtils.getString("BMUsername"));
		CPQ_Username.sendKeys(TestDataUtils.getString("BMUsername"));
		CPQ_Password.sendKeys(TestDataUtils.getString("BMPassword"));
		CPQ_Loginbtn.click();
		verifyCPQLandingPage();
	}

	public void LoginToOSS()
	{
		System.out.println("User name :: "+TestDataUtils.getString("OSSUsername"));
		oss_Loc.getOSS_UserName().sendKeys(TestDataUtils.getString("OSSUsername"));
		oss_Loc.getOSS_password().sendKeys(TestDataUtils.getString("OSSPassword"));
		oss_Loc.getOSS_LoginBtn().click();
		verifyCPQLandingPage();
	}
	public void Set_Search_account_values(){

	}

	public void hoverElement(WebElement webElement, int timeout) throws Exception {
		WaitUtils.waitForDisplayed(webElement);
		WaitUtils.waitForElementToBeClickable(webElement);

		if (webElement.isDisplayed()) {
			Actions builder = new Actions(getDriver());
			builder.moveToElement(webElement);
			builder.build().perform();
			try {
				webElement.sendKeys("");
			} catch (Exception e) {
			}
			logger.info("move to element ''" + webElement);
		} else {
			Exception error = new Exception(webElement.getText());
			logger.error("Current page does not contain element  '" + webElement.getText());
			logger.error("Error Message :" + error.getMessage());
			throw error;
		}

	}
}

package com.scholastic.qtc.pages;

import com.applitools.eyes.Eyes;
import com.applitools.eyes.MatchLevel;
import com.applitools.eyes.RectangleSize;
import com.applitools.eyes.StitchMode;
import com.scholastic.qtc.support.ApplitoolsUtils;
import com.scholastic.torque.common.BaseTestPage;
import com.scholastic.torque.common.TestBase;
import com.scholastic.torque.common.TestBaseProvider;
import com.scholastic.torque.common.TestPage;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.log4testng.Logger;

import java.util.HashMap;
import java.util.Map;

public class VisualTestingPage extends BaseTestPage<TestPage> {

	public static Map<String, Eyes> eyeMap = new HashMap<String, Eyes>();
	Logger log = Logger.getLogger(String.class);
	TestBase testBase = TestBaseProvider.getTestBase();

	public void SettingUpVisualTest(String mode) {

		String session = testBase.getContext().getProperty("SESSION").toString();
		String scenario = testBase.getContext().getProperty("SCENARIO_NAME").toString();
		System.out.println("session::: " + session);
		System.out.println("scenario::: " + scenario);
		// eyes.setApiKey(testBase.getContext().getString("applitools.apiKey"));
		// eyes.setBatch(batch);]\
		// ApplitoolsUtils applitoolsUtils=new ApplitoolsUtils();
		// ApplitoolsUtils.setEyeObject(ApplitoolsUtils.getEyes());
		Eyes eyes = ApplitoolsUtils.getEyeObject();
		System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
		System.out.println("Eyes Object:" + eyes);
		if (mode.equalsIgnoreCase("strict")) {
			eyes.setMatchLevel(MatchLevel.STRICT);
		} else if (mode.equalsIgnoreCase("exact")) {
			eyes.setMatchLevel(MatchLevel.EXACT);
		} else if (mode.equalsIgnoreCase("content")) {
			eyes.setMatchLevel(MatchLevel.CONTENT);
		} else if (mode.equalsIgnoreCase("layout")) {
			eyes.setMatchLevel(MatchLevel.LAYOUT2);
		}
		eyes.setForceFullPageScreenshot(true);

		// eyes.setHideScrollbars(true);
		// eyes.setBaselineName(scenario + "_"
		// +
		// TestBaseProvider.getTestBase().getContext().getString("driver.name"));
		if (!session.equalsIgnoreCase("")

				&& !testBase.getContext().getString("sauce").equalsIgnoreCase("false")) {
			if (TestBaseProvider.getTestBase().getContext().getString("driver.name").equalsIgnoreCase("Android")
					|| TestBaseProvider.getTestBase().getContext().getString("driver.name").equalsIgnoreCase("IOs")
					|| (TestBaseProvider.getTestBase().getContext().getString("isIpad") != null && TestBaseProvider
							.getTestBase().getContext().getString("isIpad").equalsIgnoreCase("true"))) {
				eyes.open(TestBaseProvider.getTestBase().getDriver(), "Visual Test", scenario);
			} else {
				eyes.open(TestBaseProvider.getTestBase().getDriver(), "Visual Test", scenario,
						new RectangleSize(
								Integer.parseInt(
										TestBaseProvider.getTestBase().getContext().getString("viewPortSize_width")),
								Integer.parseInt(
										TestBaseProvider.getTestBase().getContext().getString("viewPortSize_height"))));
			}
		}

		else {
			if (TestBaseProvider.getTestBase().getContext().getString("driver.name").equalsIgnoreCase("Android")
					|| TestBaseProvider.getTestBase().getContext().getString("driver.name").equalsIgnoreCase("IOs")) {
				eyes.open(TestBaseProvider.getTestBase().getDriver(), "Visual Test", scenario);
			} else {
				System.out.println("opening the eyes........");
				eyes.open(TestBaseProvider.getTestBase().getRemoteDriver(), "Visual Test", scenario,
						new RectangleSize(
								Integer.parseInt(
										TestBaseProvider.getTestBase().getContext().getString("viewPortSize_width")),
								Integer.parseInt(
										TestBaseProvider.getTestBase().getContext().getString("viewPortSize_height"))));
			}
			// eyes.setBatch(batch);
		}

		eyeMap.put("APPLITOOL_EYE", eyes);
	}

	public void tearingDownVisualTest() {
		if (!testBase.getContext().getString("applitools").equalsIgnoreCase("false")) {
			getEyes().close();
		}
	}

	public Eyes getEyes() {
		return eyeMap.get("APPLITOOL_EYE");
	}

	public void getURL(String page) {
		if (page.equals("video")) {
			System.out.println("GeT URL :: " + getTestBase().getString("video.detail.page") + "game :: "
					+ getTestBase().getString("game.detail.page") + " news  :: "
					+ getTestBase().getString("news.detail.page"));
			getDriver().get(getTestBase().getString("video.detail.page"));
		} else if (page.equals("games")) {
			getDriver().get(getTestBase().getString("game.detail.page"));
		} else if (page.equals("news")) {
			getDriver().get(getTestBase().getString("news.detail.page"));
		}
		waitForLoaderToDismiss();
		waitForAjaxToComplete();
	}

	public void waitForAjaxToComplete() {
		try {
			(new WebDriverWait(TestBaseProvider.getTestBase().getDriver(), 60)).until(new ExpectedCondition<Boolean>() {
				@Override
				public Boolean apply(WebDriver d) {
					JavascriptExecutor js = (JavascriptExecutor) TestBaseProvider.getTestBase().getDriver();
					return (Boolean) js.executeScript("return !!window.jQuery && window.jQuery.active == 0");

				}
			});
		} catch (Exception e) {
		}
	}

	public void waitForLoaderToDismiss() {
		WebDriverWait wait = new WebDriverWait(TestBaseProvider.getTestBase().getDriver(), 30);
		try {
			wait.until(new ExpectedCondition<Boolean>() {
				@Override
				public Boolean apply(WebDriver wdriver) {
					return ((JavascriptExecutor) TestBaseProvider.getTestBase().getDriver())
							.executeScript("return document.readyState").equals("complete");
				}
			});
		} catch (Exception e) {
			System.out.println("Waiting for Loader to dismiss timed out");
		}
	}

	@Override
	protected void openPage() {
		// TODO Auto-generated method stub

	}

	public void verifyPage() {
		getEyes().setForceFullPageScreenshot(true);
		getEyes().setStitchMode(StitchMode.CSS);
		if (!testBase.getContext().getString("applitools").equalsIgnoreCase("false")) {
			getEyes().checkWindow();
		}
	}

	public void verifyRegion(WebElement ele) {
		System.out.println("ele :   " + ele);
		if (!testBase.getContext().getString("applitools").equalsIgnoreCase("false")) {
			getEyes().checkRegion(ele);
		}
	}

	public void verifyStitchedRegion(WebElement ele) {
		if (!testBase.getContext().getString("applitools").equalsIgnoreCase("false")) {
			getEyes().checkRegion(ele, true);

		}
	}
}

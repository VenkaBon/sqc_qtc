package com.scholastic.qtc.support;

import com.google.common.collect.Iterables;
import com.google.common.io.Files;
import com.scholastic.qtc.steps.cpq_Steps;
import com.scholastic.torque.common.*;
import com.scholastic.torque.webdriver.ExtendedElement;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.*;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Set;

public class CommonUtil extends BaseTestPage<TestPage> {

	static WebDriverWait wait = null;
	static Logger logger = LogManager.getLogger(CommonUtil.class);
	public static String mainWindowHandle;
	public static String genericSecondWindowHandle;

	public void pause(long seconds) {
		try {
			Thread.sleep(seconds * 1000);
		} catch (InterruptedException e) {
		}
	}

	@Override
	protected void openPage() {
		getDriver().get(getContext().getString("https://action.scholastic.com/"));
	}

	public void waitForAjaxToComplete(long... second) {
		try {
			(new WebDriverWait(TestBaseProvider.getTestBase().getDriver(), 60)).until(new ExpectedCondition<Boolean>() {
				@Override
				public Boolean apply(WebDriver d) {
					JavascriptExecutor js = (JavascriptExecutor) TestBaseProvider.getTestBase().getDriver();
					return (Boolean) js.executeScript("return !!window.jQuery && window.jQuery.active == 0");

				}
			});
		} catch (Exception e) {
		}
	}

	public void waitForLoaderToDismiss() {

		try {
			WebDriverWait wait = new WebDriverWait(TestBaseProvider.getTestBase().getDriver(),
					Integer.parseInt(TestBaseProvider.getTestBase().getContext().getString("wait.timeout.sec")));
			wait.until(new ExpectedCondition<Boolean>() {
				@Override
				public Boolean apply(WebDriver wdriver) {
					return ((JavascriptExecutor) TestBaseProvider.getTestBase().getDriver())
							.executeScript("return document.readyState").equals("complete");
				}
			});
		} catch (Exception e) {
			System.out.println("Waiting for Loader to dismiss timed out");
		}
	}

	public static boolean searchText(String strSearchText) {
		boolean flagExist = false;

		try {
			Thread.sleep(3000);
//            flagExist = (Boolean) wait.until(ExpectedConditions.textToBePresentInElementLocated()
			flagExist = (Boolean) wait.until(ExpectedConditions.textToBePresentInElementLocated(By.tagName("*"), strSearchText));
			return flagExist;
		} catch (Exception var4) {
			return flagExist;
		}
	}

	public static void verifyText(String strExtractedData, String strIntputData) throws Exception {
		if (strExtractedData.contains(strIntputData)) ;
	}

	public void waitForPageToLoad() {
		waitForAjaxToComplete();
		waitForLoaderToDismiss();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public static void select(WebElement webElement, String value, int timeout) throws Exception {

		Thread.sleep(2000);
		if (webElement.isDisplayed()) {
			Select list = new Select(webElement);
			list.selectByValue(value);
//			list.selectByVisibleText(value);
			logger.info("Selected '" + value + "' from dropdown ''");
		} else {
			Exception error = new Exception(webElement.getText());
			logger.error("Current page does not contain element ''");

			String Str = new String(error.getMessage());
			logger.error("Error Message :" + Str);
			throw error;
		}

	}

	public static void selectFromDropdownByVisibleText(WebElement WebElement, String visibleText) {
		try {
			Select dropdown = new Select(WebElement);
			dropdown.selectByVisibleText(visibleText);
		} catch (Exception e) {
//			TestBaseProvider.getTestBase().getDriver().getScreenshotAs("Product Dropdown.png");
			logger.info("No dropdown value with matching text: " + visibleText);
		}
	}
	public static boolean verifyDataInTableColumns(WebElement Webelement, String strLabelName, String strValue) throws Exception {
		int col_num = 0, matchIndex = 0;
		boolean flag = false;
		List<WebElement> tableRows = Webelement.findElements(By.xpath("./tbody/tr"));

		for (WebElement trElement : tableRows) {
			List<WebElement> Cols = trElement.findElements(By.tagName("td"));
			for (WebElement tdElement : Cols) {
				col_num++;
				if (tdElement.getText().trim().contains(strLabelName)) {
					matchIndex = col_num + 1;
				} else if (matchIndex == col_num) {
					String colValue = replaceSpecialChar(tdElement.getText().trim());
					strValue = replaceSpecialChar(strValue.trim());

					if (colValue.contains(strValue) || strValue.contains(colValue) || colValue.equalsIgnoreCase(strValue)) {
						matchIndex = 0;
						flag = true;
						return flag;
					} else {
						flag = false;
						return flag;
					}
				}
			}
		}
		if (flag == false)
			return flag;
		return flag;
	}

	public static String replaceSpecialChar(String strData) {
		if (strData.contains(",")) {
			strData = strData.replace(",", "");
		}
		if (strData.contains("'")) {
			strData = strData.replace("'", "");
		}
		if (strData.contains("$")) {
			strData = strData.replace("$", "");
		}
		if (strData.contains(".")) {
			strData = strData.replace(".", "");
		}
		if (strData.contains("%")) {
			strData = strData.replace("%", "");
		}
		if (strData.equalsIgnoreCase("false")) {
			strData = "No/off/false";
		}
		if (strData.equalsIgnoreCase("true")) {
			strData = "Yes/on/true";
		}

		if (strData.equals("Yes")) {
			strData = "true";
		}
		if (strData.equals("No")) {
			strData = "false";
		}
		return strData;
	}

	public static void switchToThirdWindow() {
		TestBaseProvider.getTestBase().getDriver().switchTo().window(mainWindowHandle);
		for (int i = 0; i < 20; i++) {
			Set<String> windowHandles = TestBaseProvider.getTestBase().getDriver().getWindowHandles();
			if (CollectionUtils.isNotEmpty(windowHandles)) {
				mainWindowHandle = Iterables.get(windowHandles, 0, null);
				String thirdWindowHandle = Iterables.get(windowHandles, 2, null);

				if (StringUtils.isNotEmpty(thirdWindowHandle)) {
					TestBaseProvider.getTestBase().getDriver().switchTo().window(thirdWindowHandle);
					TestBaseProvider.getTestBase().getDriver().switchTo().window(thirdWindowHandle).manage().window().setPosition(new Point(0, 0));
					TestBaseProvider.getTestBase().getDriver().switchTo().window(thirdWindowHandle).manage().window().setSize(new Dimension(1500, 800));
					return;
				} else {
					logger.warn(i + " : There is no third browser window found");
				}
			}
		}
	}

	public static void switchToFirstWindow() {
		Set<String> windowHandles = TestBaseProvider.getTestBase().getDriver().getWindowHandles();
		if (CollectionUtils.isNotEmpty(windowHandles)) {
			mainWindowHandle = Iterables.get(windowHandles, 0, null);

			if (StringUtils.isNotEmpty(mainWindowHandle)) {
				TestBaseProvider.getTestBase().getDriver().switchTo().window(mainWindowHandle);
			} else {
				logger.warn("There is no second browser window found");
			}
		}
	}

	public static WebElement waitForElement(By by) throws InterruptedException {
		WebElement visible = null;
		long maxTime = 15 * 100; // time in milliseconds
		long waitTime = 350;
		long elapsedTime = 0;
		do {
			try {
				Thread.sleep(waitTime);
				elapsedTime += waitTime;
				visible = TestBaseProvider.getTestBase().getDriver().findElement(by);
				if (!visible.isDisplayed()) {
					visible = null;
				} else {
					logger.info("element visible" + by);
				}
			} catch (NoSuchElementException e) {
				logger.info("waiting for element to be visible" + by);
			}
		} while (visible == null && elapsedTime < maxTime);
		return visible;
	}

	public static void switchToFrame() throws InterruptedException {
		try {
			waitForElement(By.tagName("iframe"));
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			logger.info(e.getMessage());
			throw new InterruptedException();
		}
		String iframeID = TestBaseProvider.getTestBase().getDriver().findElement(By.tagName("iframe")).getAttribute("id");
		TestBaseProvider.getTestBase().getDriver().switchTo().frame(iframeID);
	}

	public static void switchToSecondWindow() {
		for (int i = 0; i < 20; i++) {
			Set<String> windowHandles = TestBaseProvider.getTestBase().getDriver().getWindowHandles();
			if (CollectionUtils.isNotEmpty(windowHandles)) {
				mainWindowHandle = Iterables.get(windowHandles, 0, null);
				String secondWindowHandle = Iterables.get(windowHandles, 1, null);
				genericSecondWindowHandle = secondWindowHandle;

				if (StringUtils.isNotEmpty(secondWindowHandle)) {
					TestBaseProvider.getTestBase().getDriver().switchTo().window(secondWindowHandle);
					try {
						TestBaseProvider.getTestBase().getDriver().switchTo().window(secondWindowHandle).manage().window().maximize();
					} catch (Exception e) {
						logger.info(e.getMessage());
					}
					return;
				} else {
					logger.warn(i + " : There is no second browser window found");
				}
			}
		}
	}

	public static void waitTillDivisionLoads() throws Throwable {
		Thread.sleep(5000);
	}
	public static String takeScreenshot(String filename, String msg) {
		File dir = new File("target/screenshots");
		dir.mkdirs();
		SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy_hhmmss");
		Date curDate = new Date();
		String strDate = sdf.format(curDate);
		filename = filename.replace(".png", "");
		filename = filename + strDate + ".png";
		File file = new File(dir, filename);
		try {
			takeScreenshot(file);
			logger.info("Wrote screen shot to "+ file.getAbsolutePath());
		} catch (IOException e) {
			logger.info("Unable to write screen shot to " + file.getAbsolutePath()+ " "+e.getMessage());
		}
		return msg;
	}
	public static void takeScreenshot(File imageFile) throws IOException {
		TakesScreenshot takesScreenshot = (TakesScreenshot) TestBaseProvider.getTestBase().getDriver();
		File src = takesScreenshot.getScreenshotAs(OutputType.FILE);
		try {
			Files.copy(src, imageFile);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.info(e.getMessage());
			throw new IOException();
		}
	}
	public static void hoverElement(WebElement webElement, int timeout) throws Throwable {
		WaitUtils.waitForElementToBeClickable((ExtendedElement) webElement);
		WaitUtils.waitForDisplayed(webElement);
		if (webElement.isDisplayed()) {
			Actions builder = new Actions(TestBaseProvider.getTestBase().getDriver());
			builder.moveToElement(webElement);
			builder.build().perform();
			try {
				webElement.sendKeys("");
			} catch (Exception e) {
			}
			logger.info("move to element ''" + webElement);
		} else {
			Exception error = new Exception(webElement.getText());
			logger.error("Current page does not contain element  '" + webElement.getText());
			logger.error("Error Message :" + error.getMessage());
			throw error;
		}

	}

	public static void focusWebElement(WebElement element) {
		((JavascriptExecutor) TestBaseProvider.getTestBase().getDriver()).executeScript("arguments[0].scrollIntoView();", element);
		try {
			((JavascriptExecutor) TestBaseProvider.getTestBase().getDriver()).executeScript("arguments[0].scrollIntoView();", element);
			moveToElement(element, 1);
		} catch (Exception e) {
			try {
				element.sendKeys("");
			} catch (Exception e2) {
				logger.info(e2.getMessage());
			}
		} finally {
			try {
				element.sendKeys("");
			} catch (Exception e2) {
			}
			try {
				moveToElement(element, 1);
			} catch (Exception e1) {
			}
		}
	}

	public static void moveToElement(WebElement webElement, int timeout) throws Exception {
		try {
			Actions builder = new Actions(TestBaseProvider.getTestBase().getDriver());
			builder.moveToElement(webElement);
			builder.build().perform();
			//logger.info("move to element ''" + webElement);
		} catch (Exception e) {
			//logger.info("unable move to element ''" + webElement);
		}
	}
}
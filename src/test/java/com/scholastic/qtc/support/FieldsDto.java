/*
 *  RL Fusion - One Stop Automation
 *
 * Copyright (c) 2019-2020 <<RL Fusion>> RL
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of RELEVANCE LAB
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with RELEVANCE LAB.
 *
 */
package com.scholastic.qtc.support;

import java.io.Serializable;

/**
 * This is the class which handle to read the data from feature file
 * @author : Rl Fusion QA
 * @version : 1.0
 * @since : 2019-06-10
 * @see : Serializable
 */

public class FieldsDto implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
    private String name;
    private String value;
	/**
	 * This method has been created for read the data Name in feature file
	 * @author : Rl Fusion QA
	 * @param : Nothing
	 * @return : Nothing.
	 * @throws : Nothing
	 */
	public String getName()
	{
        return name;
    }
	/**
	 * This method has been created for Set the data name in feature file
	 * @author : Rl Fusion QA
	 * @param : Nothing
	 * @return : Nothing.
	 * @throws : Nothing
	 */
    public void setName(String name) {
        this.name = name;
    }
	/**
	 * This method has been created for Read the data value in feature file
	 * @author : Rl Fusion QA
	 * @param : Nothing
	 * @return : Nothing.
	 * @throws : Nothing
	 */
    public String getValue()
	{
        return value;
    }
	/**
	 * This method has been created for Set the data value in feature file
	 * @author : Rl Fusion QA
	 * @param : Nothing
	 * @return : Nothing.
	 * @throws : Nothing
	 */
    public void setValue(String value)
	{
        this.value = value;
    }

    @Override
    public String toString() {
        return "Field{" +
                "name='" + name + '\'' +
                ", value='" + value + '\'' +
                '}';
    }

    public FieldsDto(String name, String value) {
        this.name = name;
        this.value = value;
    }

    public FieldsDto() {
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FieldsDto other = (FieldsDto) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}
}

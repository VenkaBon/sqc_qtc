package com.scholastic.qtc.support;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

public class Log4JTestBase {
	
private static final Logger logger = Logger.getLogger(Log4JTestBase.class.getName());
	

	public void loadlog4J(){
		String log4jConfPath = System.getProperty("user.dir")+"/sqc_qtc/src/test/resources/log4j.properties";
		PropertyConfigurator.configure(log4jConfPath);
	}

}

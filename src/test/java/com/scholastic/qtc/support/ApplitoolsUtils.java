package com.scholastic.qtc.support;

import com.applitools.eyes.BatchInfo;
import com.applitools.eyes.Eyes;
import com.scholastic.qtc.steps.Hooks;
import com.scholastic.torque.common.TestBase;
import com.scholastic.torque.common.TestBaseProvider;

public class ApplitoolsUtils {

	public static final String APPLITOOLS = "applitools";
	public static final String APPLITOOLS_API_KEY = "applitools.apiKey";
	public static final String APPNAME = "applitools.appName";
	public static final String APPLITOOLS_BASELINE_NAME = "applitools.baselineName";
	public static final String APPLITOOLS_HIDE_SCROLLBAR = "applitools.hideScrollbars";
	public static final String APPLITOOLS_MATCHLEVEL = "applitools.matchLevel";
	static TestBase testBase = TestBaseProvider.getTestBase();
	private static final String MY_MAP = "myMap";
	public static Eyes eyes;
	public static BatchInfo batch;

//	public static Eyes getEyes() {
//		// Eyes eyes = null;
//		Hooks hooks = new Hooks();
//		// eyesParams(); // To-do
//		eyes = hooks.myMap.get(MY_MAP);
//		/*
//		 * eyes = new Eyes(); Eyes eyes = new Eyes();
//		 * eyes.setApiKey(testBase.getContext().getString( "applitools.apiKey"));
//		 * System.out.println("api key" +
//		 * testBase.getContext().getString("applitools.apiKey")); eyes.setBatch(batch);
//		 * System.out.println("Batch Id: " + TestBaseProvider.getTestBase().getContext()
//		 * .getString("batchId"));
//		 */
//
//		return eyes;
//	}

	public static void setEyeObject(Eyes e) {
		System.out.println(e + "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
		// eyes= new Eyes();
		eyes = e;
	}

	public static Eyes getEyeObject() {

		return eyes;
	}

	public static void eyesParams() {
		// To do
		/*
		 * String applitoolsEnabled =
		 * TestBaseProvider.getTestBase().getString(APPLITOOLS); WebDriver driver =
		 * TestBaseProvider.getTestBase().getDriver(); if
		 * (applitoolsEnabled.equalsIgnoreCase("true")) { eyes = new Eyes();
		 * eyes.setApiKey(TestBaseProvider.getTestBase().getString(
		 * APPLITOOLS_API_KEY));
		 * eyes.setBaselineName(TestBaseProvider.getTestBase().getString(
		 * APPLITOOLS_BASELINE_NAME)); String matchLevel =
		 * TestBaseProvider.getTestBase().getString(APPLITOOLS_MATCHLEVEL); if
		 * (matchLevel.equalsIgnoreCase("strict")) {
		 * eyes.setMatchLevel(MatchLevel.STRICT); } else if
		 * (matchLevel.equalsIgnoreCase("content")) {
		 * eyes.setMatchLevel(MatchLevel.CONTENT); } else if
		 * (matchLevel.equalsIgnoreCase("layout")) {
		 * eyes.setMatchLevel(MatchLevel.LAYOUT2); } String hideScrollbars =
		 * TestBaseProvider.getTestBase().getString(APPLITOOLS_HIDE_SCROLLBAR); if
		 * (hideScrollbars.contentEquals("true")) { eyes.setHideScrollbars(true); } else
		 * { eyes.setHideScrollbars(false); } } else {
		 * System.out.println("Applitools parameter is not false"); } return eyes;
		 */

	}
}

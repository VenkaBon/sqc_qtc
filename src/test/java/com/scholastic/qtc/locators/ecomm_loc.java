package com.scholastic.qtc.locators;

import com.scholastic.torque.common.BaseTestPage;
import com.scholastic.torque.common.TestPage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public abstract class ecomm_loc extends BaseTestPage<TestPage> {

    @FindBy(xpath = "//a[@id='regLink']")
    private WebElement tech_register_lnk;
    @FindBy(xpath = "//form[@name='personalInfoForm']//*[contains(@class,'row')]//*[contains(@class,'col-xs-12')]//input")
    private List<WebElement> RegisterScreenTextBox;
    @FindBy(xpath = "//*[@id=\"nextButton\"]")
    private WebElement RegisterNextButton;
    @FindBy(xpath = ".//form[contains(@name,'password_choose')]//*[@class='col-xs-12 no-gutter']//input[@type='password']")
    private List<WebElement> RegisterCreatePasswordScreen;
    @FindBy(xpath = ".//*[contains(@class,'form-control roles dropdown-toggle')]")
    private WebElement RegisterSelectRoleDropdown;
    @FindBy(xpath = ".//*[contains(@class,'dropdown-menu role-dropdown')]//li//a")
    private List<WebElement> RegisterSelectRoleDropdownOptions;
    @FindBy(xpath = "/html/body/div[3]/div[1]/div/div[1]/div/form/div[6]/div/div[1]/label")
    private List<WebElement> RegisterCreatePasswordScreenCheckBox;
    @FindBy(xpath = "//*[@class='ASO-AEM-eCommerce teachables non-sub-tcb tour-teachersTour-element tour-teachersTour-0-element']//button[@class='close']")
    private WebElement WelcomePopupClose;
    @FindBy(xpath = "//*[@class='popover tour orphan tour-teachersTour tour-teachersTour-9 fade top in']//button[@class='close']")
    private WebElement WelcomePopupCloseScreen2;
    @FindBy(xpath = ".//*[contains(@class,'dropdown-menu grades-dropdown')]//li//a")
    public List<WebElement> RegisterTeacherGradeDropdownOptions;
    @FindBy(xpath = ".//*[contains(@class,'form-control gradename dropdown-toggle grade-placeholder')]")
    public List<WebElement> RegisterTeacherGradeDropdown;
    @FindBy(xpath = "//*[contains(@name,'zipcode')]")
    public WebElement RegisterSchoolZipcode;
    @FindBy(xpath = "//button[@id='btnSubmitOrder']")
    private WebElement Submitbutton;
    @FindBy(xpath = "//*[@id=\"termsCheckLabel\"]")
    private WebElement termscheckbox;
    @FindBy(xpath = "//button[@id='btnSaveCC']")
    private WebElement ccSavebutton;

    @FindBy(xpath = "//*[contains(@ng-model,'selectedName')]")
    public WebElement RegisterSelectSchoolDropdown;
    @FindBy(xpath = "//*[contains(@class,'dropdown-menu expanded-dropdown school')]//li//a")
    public List<WebElement> RegisterSelectSchoolDropdownOptions;
    @FindBy(xpath = "//*[contains(@class,'mysch-btn mysch-reg-btn btn-prev')]")
    public WebElement RegisterScreenPreviousButton;
    @FindBy(xpath = "//*[contains(@class,'mysch-btn btn-prev-nav')]")
    public WebElement RegisterPreviousButton;
    @FindBy(xpath = ".//button[contains(@class,'mysch-btn btn-nxt')]")
    public WebElement SuccessfullyRegisteredScreenNextCTA;
    @FindBy(xpath = ".//button[contains(@class,'mysch-btn btn-prev-nav')]")
    public WebElement SuccessfullyRegisteredScreenPreviousCTA;
    @FindBy(xpath = "//*[@class='row heading']")
    public WebElement SuccessfullyRegisteredScreenHeader;
    @FindBy(xpath = "//*[@class='col-xs-12 rte-message']//p")
    public WebElement SuccessfullyRegisteredScreenHeaderDescription;
    @FindBy(xpath = "//body//li[2]")
    private WebElement POradiobtn;
    @FindBy(xpath = "//input[@id='poNumber']")
    private WebElement POnumber;
    @FindBy(xpath = "//button[@id='btnContinue']")
    private WebElement Continuebtn;
    @FindBy(xpath="//*[@id=\"firstName_find\"]")
    public WebElement MyAccountSearchFirstName;
    @FindBy(xpath="//input[@id='lastName_find']")
    public WebElement MyAccountSearchLastName;
    @FindBy(xpath="//input[@id='UCN_find']")
    public WebElement MyAccountSearchUNC;
    @FindBy(xpath="//input[@id='email_find']")
    public WebElement MyAccountSearchEmail;
    @FindBy(xpath="//input[@id='_shipTo_t_first_name']")
    public WebElement ShipToFirstName;
    @FindBy(xpath="//input[@id='_bill_To_t_first_name']")
    public WebElement BillToFirstName;
    @FindBy(xpath="//input[@id='_soldTo_t_first_name']")
    public WebElement SoldToFirstName;
    @FindBy(xpath = "//a[@class='sch-global-register']")
    private WebElement MyScholasticregister;
    @FindBy(xpath = "//*[@id=\"contentcolumn\"]/div[1]/div[3]/div/div[1]/div[5]/div/div/div[2]/div/div[1]/div[1]")
    private WebElement MyScholasticProfileicon;
    @FindBy(xpath = "//*[contains(@name,'schoolSelect')]//label[contains(@for,'mysch-checkboxID')]")
    public List<WebElement> SchoolTypeSelectionRadioButtons;
    @FindBy(xpath = "//span[@class='order-number']")
    private WebElement webordnumber;
    @FindBy(xpath = "//body//li[1]")
    private WebElement CCradiobtn;
    @FindBy(xpath = "//input[@id='ccFirstName']")
    private WebElement ecomm_cc_firstname;
    @FindBy(xpath = "//input[@id='ccLastName']")
    private WebElement ecomm_cc_Lastname;
    @FindBy(xpath = "//input[@id='ccAddr1']")
    private WebElement ecomm_cc_Addr1;
    @FindBy(xpath = "//input[@id='ccAddr2']")
    private WebElement ecomm_cc_Addr2;
    @FindBy(xpath = "//input[@id='ccCity']")
    private WebElement ecomm_cc_City;
    @FindBy(xpath = "//select[@id='ccState']")
    private WebElement ecomm_cc_State;
    @FindBy(xpath = "//input[@id='ccZip']")
    private WebElement ecomm_cc_Zip;
    @FindBy(xpath = "//input[@id='ccPhone']")
    private WebElement ecomm_cc_Phone;
    @FindBy(xpath = "//input[@id='ccNumber']")
    private WebElement ecomm_cc_cnumber;
    @FindBy(xpath = "//select[@id='ccMonth']")
    private WebElement ecomm_cc_Month;
    @FindBy(xpath = "//select[@id='ccYear']")
    private WebElement ecomm_cc_Year;
    @FindBy(xpath = "//input[@id='ccCvn']")
    private WebElement ecomm_cc_cvn;
    @FindBy(xpath = "//div[contains(text(),'Payment Method')]")
    private WebElement ecomm_paymenthod_text;

    public WebElement getecomm_paymenthod_text() {return ecomm_paymenthod_text;}
    public WebElement getecomm_cc_cvn() {return ecomm_cc_cvn;}
    public WebElement getccSavebutton() {return ccSavebutton;}
    public WebElement getecomm_cc_firstname() {return ecomm_cc_firstname;}
    public WebElement getecomm_cc_Lastname() {return ecomm_cc_Lastname;}
    public WebElement getecomm_cc_Addr1() {return ecomm_cc_Addr1;}
    public WebElement getecomm_cc_Addr2() {return ecomm_cc_Addr2;}
    public WebElement getecomm_cc_State() {return ecomm_cc_State;}
    public WebElement getecomm_cc_City() {return ecomm_cc_City;}
    public WebElement getecomm_cc_Zip() {return ecomm_cc_Zip;}
    public WebElement getecomm_cc_Phone() {return ecomm_cc_Phone;}
    public WebElement getecomm_cc_cnumber() {return ecomm_cc_cnumber;}
    public WebElement getecomm_cc_Month() {return ecomm_cc_Month;}
    public WebElement getecomm_cc_Year() {return ecomm_cc_Year;}
    public WebElement getCCradiobtn() {return CCradiobtn;}
    public WebElement getwebordnumber() {return webordnumber;}
    public WebElement getMyScholasticProfileicon() {return MyScholasticProfileicon;}
    public WebElement getShipToFirstName() {
        return ShipToFirstName;
    }
    public WebElement getBillToFirstName() {
        return BillToFirstName;
    }
    public WebElement getSoldToFirstName() {
        return SoldToFirstName;
    }
    public List<WebElement> getSchoolTypeSelectionRadioButtons() {return SchoolTypeSelectionRadioButtons;}
    public WebElement getMyAccountSearchFirstName() {return MyAccountSearchFirstName;}
    public WebElement getMyAccountSearchLastName() {return MyAccountSearchLastName;}
    public WebElement getMyAccountSearchUNC() {return MyAccountSearchUNC;}
    public WebElement getMyAccountSearchEmail() {return MyAccountSearchEmail;}
    public WebElement getContinuebtn() {
        return Continuebtn;
    }
    public WebElement getPOnumber() {return POnumber;}
    public WebElement getMyScholasticregister() {return MyScholasticregister;}
    public WebElement getPOradiobtn() {return POradiobtn;}
    public WebElement getSuccessfullyRegisteredScreenHeaderDescription() {return SuccessfullyRegisteredScreenHeaderDescription;}
    public WebElement getSuccessfullyRegisteredScreenHeader() {return SuccessfullyRegisteredScreenHeader;}
    public WebElement getSuccessfullyRegisteredScreenPreviousCTA() {return SuccessfullyRegisteredScreenPreviousCTA;}
    public WebElement getSuccessfullyRegisteredScreenNextCTA() {return SuccessfullyRegisteredScreenNextCTA;}
    public List<WebElement> getRegisterSelectSchoolDropdownOptions() {return RegisterSelectSchoolDropdownOptions;}
    public WebElement getRegisterSelectSchoolDropdown() {return RegisterSelectSchoolDropdown;}
    public WebElement gettermscheckbox() {return termscheckbox;}
    public WebElement getSubmitbutton() {return Submitbutton;}
    public WebElement getRegisterSchoolZipcode() {return RegisterSchoolZipcode;}
    public List<WebElement> getRegisterTeacherGradeDropdownOptions() {return RegisterTeacherGradeDropdownOptions;}
    public List<WebElement> getRegisterTeacherGradeDropdown() {return RegisterTeacherGradeDropdown;}
    public List<WebElement> getRegisterCreatePasswordScreenCheckBox() {return RegisterCreatePasswordScreenCheckBox; }
    public List<WebElement> getRegisterScreenTextBox() {return RegisterScreenTextBox;}
    public WebElement getRegisterNextButton() {return RegisterNextButton;}
    public List<WebElement> getRegisterCreatePasswordScreen() {return RegisterCreatePasswordScreen;}
    public WebElement gettechregisterlnk() { return tech_register_lnk;}
    public  List<WebElement> getRegisterRegisterSelectRoleDropdownOptions() {return RegisterSelectRoleDropdownOptions;}
    public WebElement getRegisterSelectRoleDropdown() {return RegisterSelectRoleDropdown;}
    public WebElement getWelcomePopupClose() {return WelcomePopupClose;}
    public WebElement getWelcomePopupCloseScreen2() {return WelcomePopupCloseScreen2;}

}

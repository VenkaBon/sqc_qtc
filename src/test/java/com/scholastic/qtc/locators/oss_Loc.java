package com.scholastic.qtc.locators;

import com.scholastic.torque.common.BaseTestPage;
import com.scholastic.torque.common.TestPage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public abstract class oss_Loc extends BaseTestPage<TestPage>{
	@FindBy(xpath="//input[@id='_FOpt1:_FOr1:0:_FONSr2:0:_FOTr0:0:pt1:ls1:searchf:srchit::content']")
	public static WebElement OsseditSearchSubscriptionID;
	@FindBy(xpath="//div[@id='_FOpt1:_FOr1:0:_FONSr2:0:_FOTr0:0:pt1:ls1:lsvpgl']//td[@class='x1oe']//td[4]")
	public WebElement OssImgSearchicon;
	@FindBy(xpath="//a[@id='_FOpt1:_FOr1:0:_FONSr2:0:_FOTr0:0:pt1:ls1:pc1:resId1:0:cl1']")
	public static WebElement OssSearchResultSubscriptionid;
	@FindBy(xpath="//span[@id='readonly_1_transactionID_t']")
	public WebElement QuoteOrderNumber;
	@FindBy(xpath="//span[@id='readonly_1_operatingUnit1']")
	public static WebElement QuoteBusinessUnit;
	@FindBy(xpath="//span[@id='readonly_1_quoteType_t']")
	public static WebElement QuoteType;
	@FindBy(xpath = "//select[@id='_FOpt1:_FOr1:0:_FOSritemNode_receivables_billing:0:MAnt2:1:cupt1:CManF:0:AP1:cuCustomerTypeSelectOneChoice::content']")
	private static WebElement OSS_manageCustomer_custtype_dropdown;
	@FindBy(xpath = "_FOpt1:_FOr1:0:_FOSritemNode_receivables_receivables_balances:0:_FOTsdi__ReceiptsWorkArea_itemNode__FndTasksList::icon")
	private static WebElement OSS_ManageCustomer_lnk;
	@FindBy(xpath="//div[@id='_FOpt1:_FOr1:0:_FOSritemNode_receivables_receivables_balances:0:_FOTsr1:0:AP1:SPtb1::oc']//table[@class='xj0']")
	private  static WebElement OSS_ManageCustomer_btn;
	@FindBy(xpath = "//h1[contains(@class,'xnq')]")
	private static WebElement OSS_AccountReceviables_header;
	@FindBy(xpath = "//h1[contains(@class,'xnq')]")
	private static WebElement OSS_ManageCustomer_header;
	@FindBy(xpath = "//h1[contains(@class,'xnq')]']")
	private static WebElement OSS_manageCustomer_title;
	@FindBy(xpath = "//input[@id='userid']")
	private static WebElement OSS_UserName;
	@FindBy(xpath = "//input[@id='password']")
	private static WebElement OSS_password;
	@FindBy(xpath = "//button[@id='btnActive']")
	private static WebElement OSS_LoginBtn;
	@FindBy(xpath = ".//*[@*='itemNode_SubscriptionManagement_SubscriptionManagement']")
	private static WebElement OSS_SubscriptionManagementBtn;
	@FindBy(xpath = "//*[@id='pt1:_UIShome::icon']")
	private static WebElement OSS_HomeIconBtn;

	public static WebElement getOSS_HomeIconBtn() { return OSS_HomeIconBtn;}
	public static WebElement getOSS_SubscriptionManagementBtn() { return OSS_SubscriptionManagementBtn;}
	public static WebElement getOSS_LoginBtn() { return OSS_LoginBtn;}
	public static WebElement getOSS_password() { return OSS_password;}
	public static WebElement getOSS_UserName() { return OSS_UserName;}
	public static WebElement getOSSManageCustomerbtn() { return OSS_ManageCustomer_btn;}
	public static WebElement getOSSManageCustomerheader() { return OSS_ManageCustomer_header;}
	public static WebElement getOSSAccountReceviablesheader() { return OSS_AccountReceviables_header;}
	public static WebElement getOSS_ManageCustomer_lnk() {
		return OSS_ManageCustomer_lnk;
	}
	public static WebElement getOSS_manageCustomer_title() {
		return OSS_manageCustomer_title;
	}
	public static WebElement getOSS_manageCustomer_custtype_dropdown() {
		return OSS_manageCustomer_custtype_dropdown;
	}
	
	
	@Override
	protected void openPage() {
		// TODO Auto-generated method stub

	}
}

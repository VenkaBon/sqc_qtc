package com.scholastic.qtc.locators;

import com.scholastic.torque.common.BaseTestPage;
import com.scholastic.torque.common.TestPage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public abstract class cpq_Loc extends BaseTestPage<TestPage>{
	@FindBy(xpath="//span[contains(text(),'Quote/Order Details')]")
	public WebElement OrderDetails;
	@FindBy(xpath="//p[@id='selectAccount']")
	public WebElement SelectAccount;
	@FindBy(xpath="//*[@class='menuentry nav-right']//a[2]")
	public WebElement OrcQuoteTOOrderManager;
	@FindBy(xpath="//span[@class='panel-heading'][contains(text(),'Quote')]")
	public WebElement HdingQuote;
	@FindBy(xpath="//h2[contains(text(),'Oracle Quote to Order - Manager')]")
	public WebElement HdingOrcQuoteToOrderManager;
	@FindBy(xpath="//*[@id=\"firstName_find\"]")
	public WebElement MyAccountSearchFirstName;
	@FindBy(xpath="//input[@id='lastName_find']")
	public WebElement MyAccountSearchLastName;
	@FindBy(xpath="//input[@id='UCN_find']")
	public WebElement MyAccountSearchUNC;
	@FindBy(xpath="//input[@id='email_find']")
	public WebElement MyAccountSearchEmail;
	@FindBy(xpath = "//table[@class='x-grid-table-bg-color']")
	public WebElement OrdManageTable;
	@FindBy(xpath = "/html/body/div[1]/form/div/div/table/tbody/tr[1]/td/table")
	public WebElement CPQ_Searchpg_resulttable;
	@FindBy(xpath="//*[@id=\"accountsSearch\"]/tbody/tr[2]/td[1]")
	public WebElement SearchedAccount;
	@FindBy(xpath="//input[@id='_shipTo_t_first_name']")
	public WebElement ShipToFirstName;
	@FindBy(xpath="//input[@id='_bill_To_t_first_name']")
	public WebElement BillToFirstName;
	@FindBy(xpath="//input[@id='_soldTo_t_first_name']")
	public WebElement SoldToFirstName;
	@FindBy(xpath="//span[@id='readonly_1_transactionID_t']")
	public WebElement QuoteOrderNumber;
	@FindBy(xpath="//span[@id='readonly_1_operatingUnit1']")
	public WebElement QuoteBusinessUnit;
	@FindBy(xpath="//span[@id='readonly_1_quoteType_t']")
	public WebElement QuoteType;
	@FindBy(xpath="//select[@name='paymentMethod_t']")
	public WebElement QuotePaymentMethod;
	@FindBy(xpath="//input[@id='purchaseOrderNumber_t']")
	public WebElement QuotePONumber;
	@FindBy(xpath="//input[@id='startDate_Config']")
	public WebElement QuoteStartDate;
	@FindBy(xpath="//input[@id='endDate_Config']")
	public WebElement QuoteEndDate;
	@FindBy(xpath="//input[@id='term']")
	public WebElement Quoteterm;
	@FindBy(xpath="//*[@id=\"quoteExpiryDate\"]")
	public WebElement QuoteExpiryDate;
	@FindBy(xpath = "//input[@id='inputs_list36789739']")
	private WebElement CPQ_Searchpg_orderSourceno;
	@FindBy(xpath = "//input[@id='display_list36789739']")
	private WebElement CPQ_Searchpg_displaylist_orderSourceno;
	@FindBy(xpath = "//a[@id='next']")
	private WebElement CPQ_Searchpg_nextbtn;
	@FindBy(xpath = "//a[@id='next_viewSearchEditor']")
	private WebElement CPQ_Searchpg_ViewSearchEdit_nextbtn;
	@FindBy(xpath = "//select[@id='comp_36789739']")
	private WebElement CPQ_Searchpg_Comparator;
	@FindBy(xpath = "//input[@id='value_36789739']")
	private WebElement CPQ_Searchpg_Comparatorvalue;
	@FindBy(xpath = "//a[@id='search']")
	private WebElement CPQ_Searchpg_searchbtn;
	@FindBy(xpath="//span[@id='readonly_1_transactionID_t']")
	public WebElement OrdRefNo;
	@FindBy(xpath = "//span[@id='readonly_1_sourceOrderNumber']")
	public WebElement CPQ_QuoteDetailspg_OrdSourceNo;
	@FindBy(xpath="//*[@id=\"submit_for_approval\"]")
	public WebElement btnSubmitApproval;
	@FindBy(xpath="//*[@id=\"cm-manager-content\"]/table")
	public WebElement tblCMManagerContent;
	@FindBy(xpath="//*[@id=\"accountsSearch\"]/tbody/tr[2]/td[1]")
	public WebElement tblaccountsSearch;
	@FindBy(xpath="//span[@id='readonly_3_iSBN_l_36249357']")
	public WebElement lineItemadded;
	@FindBy(xpath="//table[@id='line-item-grid']")
	public WebElement tblLineItenGrid;
	@FindBy(xpath="//span[contains(text(),'HOLD Details')]")
	public WebElement lnkHolddetails;
	@FindBy(xpath="//input[@name='releaseCheckBox_credithold']")
	public WebElement rdChbxReleaseHold;
	@FindBy(xpath="//select[@name='releaseReason_creditcheck']")
	public WebElement ddropReleasecreditcheck;
	@FindBy(xpath="//input[@id='releaseComments_creditcheck']")
	public WebElement editReasoncreditcheck;
	@FindBy(xpath="//input[@name='releaseCheckBox_duphold']")
	public WebElement rdChbxReleaseduphold;
	@FindBy(xpath="//select[@name='releaseReason_dupHold']")
	public WebElement ddropReleaseReasondupHold;
	@FindBy(xpath="//input[@id='releaseComments_duphold']")
	public WebElement editReasonduphold;
	@FindBy(xpath = "//input[@id='username']")
	public WebElement CPQ_Username;
	@FindBy(xpath = "//input[@id='psword']")
	public WebElement CPQ_Password;
	@FindBy(xpath = "//a[@id='log_in']")
	public WebElement CPQ_Loginbtn;
	@FindBy(xpath = "//a[@id='new_transaction']")
	public WebElement CPQ_NewTransactionbtn;
	@FindBy(xpath = "//a[@id='add_line_item']")
    public WebElement CPQ_AddLineItem;
	@FindBy(xpath = "//a[contains(text(),'Teachables Subscriptions')]")
    private WebElement CPQ_Teachables_Subscription_btn;
    @FindBy(xpath="//*[@id=\"education-teachables\"]/a")
    public WebElement educationteachables;
    @FindBy(xpath="//*[@id=\"education-teachables\"]/ul/li/a")
    public WebElement lnkeducationteachables;
    @FindBy(xpath="//select[@id='oLDISBN']")
    public WebElement SelectDDOldISBN;
    @FindBy(xpath="//div[@id='attribute-term']//div[@class='attribute-field-container']")
    public WebElement SubScriptionTerm;
    @FindBy(xpath="//input[@id='startDate_Config']")
    public WebElement SubScriptionStDate;
    @FindBy(xpath = "//a[@id='save']")
    private WebElement CPQ_Save_btn;
    @FindBy(xpath = "//a[@id='update']")
    private WebElement CPQ_update_btn;
    @FindBy(xpath = "//*[@id=\"add_to_transaction\"]")
    private WebElement CPQ_addToTransaction_btn;
    @FindBy(xpath = "//div[contains(@class,'floating')]//table[@id='1_36246163']//tbody//tr//td[@class='button-middle']//div//a[@id='submit_for_approval']")
    private WebElement CPQ_submitForApproval_btn;
    @FindBy(xpath = "//a[@id='validation_check']")
    private WebElement CPQ_ValidationCheck;
    @FindBy(xpath = "//a[@id='cancel_approvals/revise']")
    private WebElement CPQ_CancelApprovalRevise_btn;
    @FindBy(xpath="//span[@id='readonly_1_status_t']")
    public WebElement OrdStatus;
    @FindBy(xpath="//a[contains(text(),'Teaching Resources')]")
    public WebElement HdingTeachingResources;
    @FindBy(xpath="//h3[@id='config-header']")
    public WebElement hdingModelConfiguration;
    @FindBy(xpath = "//a[@id='release_holds']")
    private WebElement CPQ_ReleaseHold_btn;
    @FindBy(xpath = "//a[@id='submit_order']")
    private WebElement CPQ_SubmitOrder_btn;
    @FindBy(xpath = "//a[@id='delete_quote']")
    private WebElement CPQ_DeleteQuote_btn;
    @FindBy(xpath = "//a[4]//img[1]")
    private WebElement CPQ_Logout;
	@FindBy(xpath = "//*[@id=\"searchAccount\"]")
	private WebElement CPQ_SearchAccount_btn;
	@FindBy(xpath = "//span[contains(text(),'This Quote is on HOLD. Please review the HOLD tab')]")
	private WebElement CPQ_HoldMessage_stm;
	@FindBy(xpath="//input[@id='_FOpt1:_FOr1:0:_FONSr2:0:_FOTr0:0:pt1:ls1:searchf:srchit::content']")
	public static WebElement OsseditSearchSubscriptionID;
	@FindBy(xpath="//div[@id='_FOpt1:_FOr1:0:_FONSr2:0:_FOTr0:0:pt1:ls1:lsvpgl']//td[@class='x1oe']//td[4]")
	public WebElement OssImgSearchicon;
	@FindBy(xpath="//a[@id='_FOpt1:_FOr1:0:_FONSr2:0:_FOTr0:0:pt1:ls1:pc1:resId1:0:cl1']")
	public static WebElement OssSearchResultSubscriptionid;
//	@FindBy(xpath="//span[@id='readonly_1_transactionID_t']")
//	public WebElement QuoteOrderNumber;
//	@FindBy(xpath="//span[@id='readonly_1_operatingUnit1']")
//	public static WebElement QuoteBusinessUnit;
//	@FindBy(xpath="//span[@id='readonly_1_quoteType_t']")
//	public static WebElement QuoteType;
	@FindBy(xpath = "//select[@id='_FOpt1:_FOr1:0:_FOSritemNode_receivables_billing:0:MAnt2:1:cupt1:CManF:0:AP1:cuCustomerTypeSelectOneChoice::content']")
	private static WebElement OSS_manageCustomer_custtype_dropdown;
	@FindBy(xpath = "_FOpt1:_FOr1:0:_FOSritemNode_receivables_receivables_balances:0:_FOTsdi__ReceiptsWorkArea_itemNode__FndTasksList::icon")
	private static WebElement OSS_ManageCustomer_lnk;
	@FindBy(xpath="//div[@id='_FOpt1:_FOr1:0:_FOSritemNode_receivables_receivables_balances:0:_FOTsr1:0:AP1:SPtb1::oc']//table[@class='xj0']")
	private  static WebElement OSS_ManageCustomer_btn;
	@FindBy(xpath = "//h1[contains(@class,'xnq')]")
	private static WebElement OSS_AccountReceviables_header;
	@FindBy(xpath = "//h1[contains(@class,'xnq')]")
	private static WebElement OSS_ManageCustomer_header;
	@FindBy(xpath = "//h1[contains(@class,'xnq')]']")
	private static WebElement OSS_manageCustomer_title;
	@FindBy(xpath = "//input[@id='userid']")
	private static WebElement OSS_UserName;
	@FindBy(xpath = "//input[@id='password']")
	private static WebElement OSS_password;
	@FindBy(xpath = "//button[@id='btnActive']")
	private static WebElement OSS_LoginBtn;
	@FindBy(xpath = ".//*[@*='itemNode_SubscriptionManagement_SubscriptionManagement']")
	private static WebElement OSS_SubscriptionManagementBtn;
	@FindBy(xpath = "//*[@id='pt1:_UIShome::icon']")
	private static WebElement OSS_HomeIconBtn;
	@FindBy(xpath = "//a[@id='search']")
	private static  WebElement CPQ_Search_btn;



	public static WebElement getCPQ_Search_btn() { return CPQ_Search_btn;}
	public static WebElement getOSS_HomeIconBtn() { return OSS_HomeIconBtn;}
	public static WebElement getOSS_SubscriptionManagementBtn() { return OSS_SubscriptionManagementBtn;}
	public static WebElement getOSS_LoginBtn() { return OSS_LoginBtn;}
	public static WebElement getOSS_password() { return OSS_password;}
	public static WebElement getOSS_UserName() { return OSS_UserName;}
	public static WebElement getOSSManageCustomerbtn() { return OSS_ManageCustomer_btn;}
	public static WebElement getOSSManageCustomerheader() { return OSS_ManageCustomer_header;}
	public static WebElement getOSSAccountReceviablesheader() { return OSS_AccountReceviables_header;}
	public static WebElement getOSS_ManageCustomer_lnk() {
		return OSS_ManageCustomer_lnk;
	}
	public static WebElement getOSS_manageCustomer_title() {
		return OSS_manageCustomer_title;
	}
	public static WebElement getOSS_manageCustomer_custtype_dropdown() {
		return OSS_manageCustomer_custtype_dropdown;
	}
	public WebElement getCPQ_HoldMessage_stm() { return CPQ_HoldMessage_stm;}
	public WebElement getCPQ_SearchAccount_btn() { return CPQ_SearchAccount_btn;}
    public WebElement getCPQ_Logout() { return CPQ_Logout;}
    public WebElement getCPQDeleteQuotebtn() { return CPQ_DeleteQuote_btn;}
    public WebElement getCPQSubmitOrderbtn() { return CPQ_SubmitOrder_btn;}
    public WebElement getCPQReleaseHoldbtn() { return CPQ_ReleaseHold_btn;}
    public WebElement gethdingModelConfiguration() { return hdingModelConfiguration;}
    public WebElement getHdingTeachingResources() { return HdingTeachingResources;}
    public WebElement getOrdStatus() { return OrdStatus;}
    public WebElement getCPQ_CancelApprovalRevise_btn() { return CPQ_CancelApprovalRevise_btn;}
    public WebElement getCPQ_ValidationCheck() { return CPQ_ValidationCheck;}
    public WebElement getCPQ_submitForApproval_btn() { return CPQ_submitForApproval_btn;}
    public WebElement getCPQ_addToTransaction_btn() { return CPQ_addToTransaction_btn;}
    public WebElement getCPQ_update_btn() { return CPQ_update_btn;}
    public WebElement getCPQ_Save_btn() { return CPQ_Save_btn;}
	public WebElement getCPQ_Username() { return CPQ_Username;}
	public WebElement getCPQ_Password() { return CPQ_Password;}
	public WebElement getCPQ_Loginbtn() { return CPQ_Loginbtn;}
	public WebElement getCPQ_NewTransactionbtn() { return CPQ_NewTransactionbtn;}
	public WebElement getSelectAccount() { return SelectAccount;}
    public WebElement getCPQ_AddLineItembtn() { return CPQ_AddLineItem;}
    public WebElement getCPQ_Teachables_Subscription_btn(){ return CPQ_Teachables_Subscription_btn;}

    public WebElement getSubScriptionStDate() {
        return SubScriptionStDate;
    }
    public WebElement getSubScriptionTerm() {
        return SubScriptionTerm;
    }
    public WebElement getSelectDDOldISBN() {return SelectDDOldISBN;}

    public WebElement geteducationteachables() {
        return educationteachables;
    }
    public WebElement getlnkeducationteachables() {
        return lnkeducationteachables;
    }
	public WebElement getCPQSearchpgsearchbtn() {return CPQ_Searchpg_searchbtn;	}
	public WebElement getCPQ_Searchpg_resulttable(){
		return CPQ_Searchpg_resulttable;
	}
	public WebElement getCPQSearchpgComparatorvalue() {
		return CPQ_Searchpg_Comparatorvalue;
	}
	public WebElement getCPQSearchpgComparator() {
		return CPQ_Searchpg_Comparator;
	}
	public WebElement getCPQSearchpgviewsearcheditnextbtn() {
		return CPQ_Searchpg_ViewSearchEdit_nextbtn;
	}
	public WebElement getCPQSearchpgnextbtn() {
		return CPQ_Searchpg_nextbtn;
	}
	public WebElement getCPQSearchpgorderSourceno() {
		return CPQ_Searchpg_orderSourceno;
	}
	public WebElement getCPQSearchpgdisplaylistorderSourceno() {
		return CPQ_Searchpg_displaylist_orderSourceno;
	}
	public WebElement getCPQ_QuoteDetailspg_OrdSourceNo() {
		return CPQ_QuoteDetailspg_OrdSourceNo;
	}
	public WebElement gettblaccountsSearch() {return tblaccountsSearch;}
	public WebElement getMyAccountSearchFirstName() {return MyAccountSearchFirstName;}
	public WebElement getMyAccountSearchLastName() {return MyAccountSearchLastName;}
	public WebElement getMyAccountSearchUNC() {return MyAccountSearchUNC;}
	public WebElement getMyAccountSearchEmail() {return MyAccountSearchEmail;}

	@Override
	protected void openPage() {
		// TODO Auto-generated method stub

	}
}


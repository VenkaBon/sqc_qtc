package com.scholastic.qtc.locators;

import com.scholastic.torque.common.BaseTestPage;
import com.scholastic.torque.common.TestPage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public abstract class qtc_admin_Loc extends BaseTestPage<TestPage> {


	@FindBy(xpath ="//span[@class='language-label l2']")
	private WebElement spanishToggleOnLandingPage;

	public WebElement getSpanishToggleOnLandingPage(){
		return spanishToggleOnLandingPage;
	}


	@Override
	protected void openPage() {
		// TODO Auto-generated method stub

	}

}

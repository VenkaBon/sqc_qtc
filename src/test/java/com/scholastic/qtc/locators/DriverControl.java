package com.scholastic.qtc.locators;

import com.scholastic.torque.common.TestBaseProvider;
import org.openqa.selenium.WebDriver;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class DriverControl {

	public static WebDriver getDriver() {
		WebDriver driver = TestBaseProvider.getTestBase().getDriver();
		return driver;

	}

	public String getValueOf(String str) throws IOException {
		FileInputStream fil = new FileInputStream("./resources/validations.properties");
		Properties pobj = new Properties();
		pobj.load(fil);
		String data = pobj.getProperty(str);
		return data;
	}
}
